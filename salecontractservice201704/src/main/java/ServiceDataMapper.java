
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import javax.enterprise.context.RequestScoped;
import javax.portlet.PortletRequest;

import dk.semlerit.autocore.util.infrastructure.ws.util.ServiceUtil;
import dk.semlerit.autocore.ws.saledocument.core.model.ContractType;
import dk.semlerit.autocore.ws.saledocument.core.model.Document;
import dk.semlerit.autocore.ws.saledocument.core.model.DocumentStatus;
import dk.semlerit.autocore.ws.saledocument.core.model.DocumentType;
import dk.semlerit.autocore.ws.saledocument.core.model.Equipment;
import dk.semlerit.autocore.ws.saledocument.core.model.EquipmentType;
import dk.semlerit.autocore.ws.saledocument.core.model.SRTradeType;
import dk.semlerit.autocore.ws.saledocument.core.model.TaxCodeType;
import dk.semlerit.autocore.ws.saledocument.core.model.VehicleCondition;
import dk.semlerit.autocore.ws.saledocument.core.model.VehicleMake;
import dk.semlerit.autocore.ws.saledocument.core.model.VehiclePurchasedByOwner;
import dk.semlerit.autocore.ws.vehiclesalecontract201704.client.PriceConfigurationType;
import dk.semlerit.autocore.ws.vehiclesalecontract201704.client.RetrieveSaleContractResponse;
import dk.semlerit.autocore.ws.vehiclesalecontract201704.client.VehiclePurchasedByOwnerEconomyType;
import dk.semlerit.autocore.ws.vehiclesalecontract201704.client.VehiclePurchasedByOwnerType;
import lombok.NonNull;

/**
 * Utility to handle mapping of data from services to sales document structure
 * 
 * @author extmr
 * @author brpe
 * @author edbmi
 * @author extthtb
 * 
 * @version 4.0
 * @since 2.0
 */
@RequestScoped
public class ServiceDataMapper {

	private VehiclePurchasedByOwner getVehiclePurchasedByOwner(
			RetrieveSaleContractResponse retrieveSaleContractResponse) {

		VehiclePurchasedByOwnerType vehiclePurchasedByOwnerType = Optional
				.ofNullable(retrieveSaleContractResponse.getVehiclePurchasedByOwner())
				.orElseThrow(() -> new IllegalStateException("VehiclePurchased by customer must not be null."));

		final VehiclePurchasedByOwner vehiclePurchasedByOwner = new VehiclePurchasedByOwner();

		vehiclePurchasedByOwner.setMake(VehicleMake.getByValue(vehiclePurchasedByOwnerType.getMake()));
		vehiclePurchasedByOwner.setVariantDocumentId(vehiclePurchasedByOwnerType.getVariantDocumentId());
		vehiclePurchasedByOwner.setVehicleId(retrieveSaleContractResponse.getVehicleOrderVehicleId());
		vehiclePurchasedByOwner.setTypeCode(vehiclePurchasedByOwnerType.getVpbcType());
		vehiclePurchasedByOwner.setEuNumberPlate(Objects.nonNull(vehiclePurchasedByOwnerType.getEuNumberPlate())
				? "Ja".equalsIgnoreCase(vehiclePurchasedByOwnerType.getEuNumberPlate())
				: null);
		vehiclePurchasedByOwner.setLicensePlate(vehiclePurchasedByOwnerType.getVpbcLicensePlate());
		vehiclePurchasedByOwner.setVin(vehiclePurchasedByOwnerType.getIdentificationNumber());
		vehiclePurchasedByOwner.setScheduleCode(vehiclePurchasedByOwnerType.getCardNumber());
		vehiclePurchasedByOwner.setInsuranceCompany(vehiclePurchasedByOwnerType.getInsuranceCompany());
		vehiclePurchasedByOwner.setInsurancePolicyNumber(vehiclePurchasedByOwnerType.getInsurancePolicyNumber());
		vehiclePurchasedByOwner.setDeliveryDate(vehiclePurchasedByOwnerType.getDeliveryDate());
		vehiclePurchasedByOwner.setDeliveryTime(vehiclePurchasedByOwnerType.getDeliveryTime());
		vehiclePurchasedByOwner.setDeliveryWeek(vehiclePurchasedByOwnerType.getDeliveryWeek());
		vehiclePurchasedByOwner.setDeliveryYear(vehiclePurchasedByOwnerType.getDeliveryYear());
		vehiclePurchasedByOwner.setDeliveryArranged(vehiclePurchasedByOwnerType.isDeliveryIsArranged());
		vehiclePurchasedByOwner
				.setVehicleCondition(VehicleCondition.getByValue(vehiclePurchasedByOwnerType.getCondition()));
		vehiclePurchasedByOwner.setModelText(vehiclePurchasedByOwnerType.getVpbcModel());
		vehiclePurchasedByOwner.setVehicleType(vehiclePurchasedByOwnerType.getVehicleType());
		vehiclePurchasedByOwner.setColor(vehiclePurchasedByOwnerType.getVpbcColor());
		vehiclePurchasedByOwner.setInterior(vehiclePurchasedByOwnerType.getInterior());
		vehiclePurchasedByOwner.setFirstRegistrationDate(vehiclePurchasedByOwnerType.getVpbcFirstRegistrationDate());
		vehiclePurchasedByOwner.setProductionYear(vehiclePurchasedByOwnerType.getProductionYear());
		vehiclePurchasedByOwner.setKilometers(vehiclePurchasedByOwnerType.getVpbcKilometers());
		vehiclePurchasedByOwner.setFactoryEquipment(vehiclePurchasedByOwnerType.getVpbcFactoryEquipment());
		vehiclePurchasedByOwner.setOrderType(vehiclePurchasedByOwnerType.getOrderType());

		final VehiclePurchasedByOwnerEconomyType vehiclePurchasedByOwnerEconomyType = Optional
				.ofNullable(vehiclePurchasedByOwnerType.getEconomy())
				.orElseThrow(() -> new IllegalStateException("VehiclePurchasedByOwnerEconomyType must not be null."));

		Optional<PriceConfigurationType> priceConfigurationType = Optional
				.ofNullable(vehiclePurchasedByOwnerEconomyType.getPriceConfiguration());

		if (priceConfigurationType.isPresent()) {
			PriceConfigurationType priceConfiguration = priceConfigurationType.get();
			vehiclePurchasedByOwner.setCarPriceIncludingVat(priceConfiguration.getCarPriceIncludingVAT());
			vehiclePurchasedByOwner.setCarPriceExcludingVat(priceConfiguration.getCarPriceExcludingVAT());
		}

		// Transfer equipment..
		vehiclePurchasedByOwner
				.setStandardEquipments(Optional.ofNullable(vehiclePurchasedByOwnerType.getStandardEquipments())
						.orElseGet(() -> Collections.emptyList()).parallelStream().map(equipmentType -> {

							return Equipment.builder().type(EquipmentType.STANDARD_EQUIPMENT)
									.code(equipmentType.getCode()).description(equipmentType.getDescription())
									.priceIncludingVAT(equipmentType.getPriceIncludingVAT())
									.priceExcludingVAT(equipmentType.getPriceExcludingVAT()).build();
						}).collect(Collectors.toList()));

		vehiclePurchasedByOwner.setStandardEquipmentDeselecteds(
				Optional.ofNullable(vehiclePurchasedByOwnerType.getStandardEquipmentDeselecteds())
						.orElseGet(() -> Collections.emptyList()).parallelStream().map(equipmentType -> {
							return Equipment.builder().type(EquipmentType.STANDARD_EQUIPMENT_DESELECTED)
									.code(equipmentType.getCode()).description(equipmentType.getDescription())
									.priceIncludingVAT(equipmentType.getPriceIncludingVAT())
									.priceExcludingVAT(equipmentType.getPriceExcludingVAT()).build();
						}).collect(Collectors.toList()));

		vehiclePurchasedByOwner
				.setAdditionalEquipments(Optional.ofNullable(vehiclePurchasedByOwnerType.getAdditionalEquipments())
						.orElseGet(() -> Collections.emptyList()).parallelStream().map(equipmentType -> {
							return Equipment.builder().type(EquipmentType.ADDITIONAL_EQUIPMENT)
									.code(equipmentType.getCode()).description(equipmentType.getDescription())
									.priceIncludingVAT(equipmentType.getPriceIncludingVAT())
									.priceExcludingVAT(equipmentType.getPriceExcludingVAT()).build();
						}).collect(Collectors.toList()));

		vehiclePurchasedByOwner.setImporterDefinedDealerEquipments(
				Optional.ofNullable(vehiclePurchasedByOwnerType.getImporterEquipments())
						.orElseGet(() -> Collections.emptyList()).parallelStream().map(equipmentType -> {
							return Equipment.builder().type(EquipmentType.IMPORTER_DEFINED_DEALER_EQUIPMENT)
									.code(equipmentType.getCode()).description(equipmentType.getDescription())
									.priceIncludingVAT(equipmentType.getPriceIncludingVAT())
									.priceExcludingVAT(equipmentType.getPriceExcludingVAT())
									.listPriceExcludingVAT(equipmentType.getListPriceExcludingVAT()).build();
						}).collect(Collectors.toList()));

		vehiclePurchasedByOwner
				.setDealerEquipments(Optional.ofNullable(vehiclePurchasedByOwnerType.getDealerEquipments())
						.orElseGet(() -> Collections.emptyList()).parallelStream().map(equipmentType -> {
							return Equipment.builder().type(EquipmentType.DEALER_EQUIPMENT)
									.code(equipmentType.getCode()).description(equipmentType.getDescription())
									.priceIncludingVAT(equipmentType.getPriceIncludingVAT())
									.priceExcludingVAT(equipmentType.getPriceExcludingVAT()).build();
						}).collect(Collectors.toList()));

		return vehiclePurchasedByOwner;
	}

	private String mapDocumentOwner(RetrieveSaleContractResponse retrieveSaleContractResponse) {
		String documentOwner = retrieveSaleContractResponse.getDocumentOwner();

		if (ServiceUtil.WSDL_MAKE_VW.equalsIgnoreCase(wsResponse.getDocumentOwner())) {
			saleContractTO.setDocumentOwner(ServiceUtil.CACHEKEY_MAKE_VW);
		} else if (ServiceUtil.WSDL_MAKE_VWVAN.equalsIgnoreCase(wsResponse.getDocumentOwner())) {
			saleContractTO.setDocumentOwner(ServiceUtil.CACHEKEY_MAKE_VWVAN);
		} else if (ServiceUtil.WSDL_MAKE_SEAT.equalsIgnoreCase(wsResponse.getDocumentOwner())) {
			saleContractTO.setDocumentOwner(ServiceUtil.CACHEKEY_MAKE_SEAT);
		} else if (ServiceUtil.WSDL_MAKE_SKODA.equalsIgnoreCase(wsResponse.getDocumentOwner())) {
			saleContractTO.setDocumentOwner(ServiceUtil.CACHEKEY_MAKE_SKODA);
		} else if (ServiceUtil.WSDL_MAKE_AUDI.equalsIgnoreCase(wsResponse.getDocumentOwner())) {
			saleContractTO.setDocumentOwner(ServiceUtil.CACHEKEY_MAKE_AUDI);
		} else if (ServiceUtil.WSDL_MAKE_PORSCHE.equalsIgnoreCase(wsResponse.getDocumentOwner())) {
			saleContractTO.setDocumentOwner(ServiceUtil.CACHEKEY_MAKE_PORSCHE);
		}
	}

	/**
	 * Returns a document mapped from a Service client response.
	 * 
	 * @param retrieveSaleContractResponse
	 * @param documentId
	 * @return Document
	 * @throws NullPointerException if params is {@literal null}
	 */
	public Document getDocument(@NonNull RetrieveSaleContractResponse retrieveSaleContractResponse, @NonNull String documentId) {

		final Document document = new Document();

		// Set basic sale contract values
		document.setId(documentId);
		document.setDocumentOwner(mapDocumentOwner(retrieveSaleContractResponse));
		
		
		
		
		
		
		
		
		document.setDocumentType(DocumentType.SALECONTRACT);
		document.setStatus(DocumentStatus.getByValue(retrieveSaleContractResponse.getStatus()));
		document.setOriginalStatus(document.getStatus());
		document.setCreatedByService(retrieveSaleContractResponse.isCreatedByService());
		document.setSellerName(retrieveSaleContractResponse.getSellerName());
		document.setSellerEmail(retrieveSaleContractResponse.getSellerEmail());
		document.setBuyerName(retrieveSaleContractResponse.getBuyerName());
		document.setFullNotesDocumentURL(retrieveSaleContractResponse.getFullNotesDocumentURL());
		document.setStatusCaseNumber(retrieveSaleContractResponse.getStatusCaseNumber());
		document.setAdditionalAgreementComments(retrieveSaleContractResponse.getAdditionalAgreementComments());
		document.setWarrantyConditions(retrieveSaleContractResponse.getWarrantyConditions());
		document.setDate(retrieveSaleContractResponse.getDate());
		document.setAcceptedMarketing(retrieveSaleContractResponse.isAcceptedMarketing());
		document.setVehicleOwnershipPOA(retrieveSaleContractResponse.isVehicleOwnershipPOA());
		document.setSaleOrderId(retrieveSaleContractResponse.getSaleOrderId());
		document.setVehicleOrderId(retrieveSaleContractResponse.getVehicleOrderId());
		document.setArchived(retrieveSaleContractResponse.isIsArchived());
		//document.setPreviousVersionDocumentID(retrieveSaleContractResponse.getPreviousVersionDocumentID());
		document.setPurchasedByOwnerEconomyDraftPresent(retrieveSaleContractResponse.isHasVehiclePurchasedByOwnerEconomyDraft());
		document.setSoldToDealerEconomyDraftPresent(retrieveSaleContractResponse.isHasVehicleSoldToDealerEconomyDraft());
		document.setLastModifiedDate(retrieveSaleContractResponse.getLastModifiedDate());

		// Set vehicle purchased by customer
		document.setVehiclePurchasedByOwner(getVehiclePurchasedByOwner(retrieveSaleContractResponse));

		document.setRemainingAmountInstitute(vehiclePurchasedByOwnerEconomyTO.getRemainingAmountInstitute());
		document.setRemainingAmountMonthlyPayment(vehiclePurchasedByOwnerEconomyTO.getRemainingAmountMonthlyPayment());
		document.setRemainingAmountAnnualInterest(vehiclePurchasedByOwnerEconomyTO.getRemainingAmountAnnualInterest());
		document.setRemainingAmountMonths(vehiclePurchasedByOwnerEconomyTO.getRemainingAmountMonths());
		document.setGrandTotalPriceType(
				GrandTotalPriceType.getByValue(vehiclePurchasedByOwnerEconomyTO.getTotalPriceType()));

		if (priceConfigurationTO != null) {
			// Set economy
			final EconomyTO economy = new EconomyTO();
			economy.setDealerMargin(priceConfigurationTO.getDealerMargin());
			economy.setSalesPriceIncludingVat(vehiclePurchasedByOwnerEconomyTO.getPriceIncludingRegistrationTax());
			economy.setSalesPriceExcludingVat(vehiclePurchasedByOwnerEconomyTO.getPrice());
			economy.setVehicleSoldToDealerTotalPrice(
					vehiclePurchasedByOwnerEconomyTO.getVehicleSoldToDealerTotalPrice());
			economy.setCustomerDiscount(priceConfigurationTO.getCustomerDiscountAmount());
			economy.setDealerMaxDiscount(priceConfigurationTO.getMaximumDiscount());
			economy.setTotalTax(priceConfigurationTO.getTotalTax());
			economy.setTotalVat(vehiclePurchasedByOwnerEconomyTO.getVat());
			economy.setGrandTotalPrice(vehiclePurchasedByOwnerEconomyTO.getTotalPrice());
			economy.setSecurityEquipmentDiscount(priceConfigurationTO.getSecurityEquipmentDIS());
			economy.setFuelRegulation(priceConfigurationTO.getFuelRegulation());
			economy.setTaxExcludingRegulations(priceConfigurationTO.getTaxExclRegulations());
			economy.setStandardEquipmentTotalPriceIncludingVat(
					priceConfigurationTO.getStandardEquipmentTotalPriceIncludingVAT());
			economy.setStandardEquipmentTotalPriceExcludingVat(
					priceConfigurationTO.getStandardEquipmentTotalPriceExcludingVAT());
			economy.setAdditionalEquipmentTotalPriceIncludingVat(
					priceConfigurationTO.getAdditionalEquipmentTotalPriceIncludingVAT());
			economy.setAdditionalEquipmentTotalPriceExcludingVat(
					priceConfigurationTO.getAdditionalEquipmentTotalPriceExcludingVAT());
			economy.setImporterDefinedDealerEquipmentTotalPriceIncludingVat(
					priceConfigurationTO.getImporterEquipmentTotalPriceIncludingVAT());
			economy.setImporterDefinedDealerEquipmentTotalPriceExcludingVat(
					priceConfigurationTO.getImporterEquipmentTotalPriceExcludingVAT());
			economy.setImporterDefinedDealerEquipmentTotalListPriceExcludingVat(
					priceConfigurationTO.getImporterEquipmentListTotalPriceExcludingVAT());
			economy.setDealerEquipmentTotalPriceIncludingVat(
					priceConfigurationTO.getDealerEquipmentTotalPriceIncludingVAT());
			economy.setDealerEquipmentTotalPriceExcludingVat(
					priceConfigurationTO.getDealerEquipmentTotalPriceExcludingVAT());
			economy.setTotalPriceIncludingVat(priceConfigurationTO.getTotalPriceIncludingVAT());
			economy.setTotalPriceExcludingVat(priceConfigurationTO.getTotalPriceExcludingVAT());
			economy.setTotalPriceExcludingTaxExcludingVat(
					vehiclePurchasedByOwnerEconomyTO.getTotalPriceExcludingTaxExcludingVat());
			economy.setTotalPriceExcludingTaxIncludingVat(
					vehiclePurchasedByOwnerEconomyTO.getTotalPriceExcludingTaxIncludingVat());
			economy.setMinimumStandardPriceTotal(priceConfigurationTO.getMinimumStandardPrice());
			economy.setTotalVehiclePriceExcludingVat(
					vehiclePurchasedByOwnerEconomyTO.getTotalVehiclePriceExcludingVat());
			economy.setTotalVehiclePriceIncludingVat(
					vehiclePurchasedByOwnerEconomyTO.getTotalVehiclePriceIncludingVat());
			economy.setTotalVehiclePriceVat(vehiclePurchasedByOwnerEconomyTO.getTotalVehiclePriceVat());

			document.setEconomyTO(economy);

			// Set price configuration
			PriceConfigurationTO priceConfiguration = document.getPriceConfigurationTO();
			if (null == priceConfiguration) {
				priceConfiguration = new PriceConfigurationTO();
				document.setPriceConfigurationTO(priceConfiguration);
			}

			priceConfiguration.setSalesPrice(vehiclePurchasedByOwnerEconomyTO.getPriceIncludingRegistrationTax());
			priceConfiguration.setSalesPriceVat(vehiclePurchasedByOwnerEconomyTO.getVat());

			final Integer deliveryExpenseTypeCode = doMigrationCheck(vehiclePurchasedByOwnerTO.getType());
			priceConfiguration
					.setDeliveryExpenseTypeCode(DeliveryExpenseTypeCodeType.getByValue(deliveryExpenseTypeCode));
			priceConfiguration.setVatIndicator(
					VatIndicatorType.getByValue(vehiclePurchasedByOwnerEconomyTO.getPriceVatIndicator()));
			priceConfiguration.setTaxIndicator(TaxIndicatorType
					.getByValue(vehiclePurchasedByOwnerEconomyTO.getPriceIncludingRegistrationTaxLabel()));

			// Compliance for older documents where the Schema definition
			// allows for null values when tax code is not selected.
			if (StringUtils.isEmpty(vehiclePurchasedByOwnerEconomyTO.getTaxCode())) {
				priceConfiguration.setTaxCode(TaxCodeType.NOT_SPECIFIED.getValue());
			} else {
				priceConfiguration.setTaxCode(vehiclePurchasedByOwnerEconomyTO.getTaxCode());
			}

			if (VehicleMakeType.isUsedVehicleMake(vehiclePurchasedByCustomer.getMake())) {
				priceConfiguration.setUsedVehicleTypeIndicator(resolveUsedVehicleTypeIndicator(
						priceConfigurationTO.getUsedVehicleTypeIndicator(), priceConfiguration));

				priceConfiguration.setEquipmentProvidedByFactory(
						vehiclePurchasedByOwnerEconomyTO.getEquipmentProvidedByFactory());
				economy.setEquipmentProvidedByDealerTotalPrice(
						vehiclePurchasedByOwnerEconomyTO.getEquipmentProvidedByDealer());
				// We need to set these in economy as well as the document
				// is not (re)calucated on retrieve
				economy.setEquipmentProvidedByFactoryTotalPrice(
						vehiclePurchasedByOwnerEconomyTO.getEquipmentProvidedByFactory());
				economy.setEquipmentProvidedByDealerTotalPrice(
						vehiclePurchasedByOwnerEconomyTO.getEquipmentProvidedByDealer());

				// Special case for Used own brand and foreign brand since
				// it is mapped differently in the Notes Service.
				economy.setTotalPriceIncludingVat(vehiclePurchasedByOwnerEconomyTO.getPriceInclVAT());
				economy.setTotalPriceExcludingVat(vehiclePurchasedByOwnerEconomyTO.getPrice());
			} else {
				economy.setEquipmentProvidedByFactoryTotalPrice(
						vehiclePurchasedByOwnerEconomyTO.getEquipmentProvidedByFactory());
				economy.setEquipmentProvidedByDealerTotalPrice(
						vehiclePurchasedByOwnerEconomyTO.getEquipmentProvidedByDealer());
			}

			priceConfiguration.setCalculationDate(vehiclePurchasedByOwnerEconomyTO.getCalculationDate());
			priceConfiguration.setPriceCode(vehiclePurchasedByOwnerEconomyTO.getPriceCode());
			priceConfiguration.setPrice1Label(vehiclePurchasedByOwnerEconomyTO.getField1());
			priceConfiguration.setPrice1(vehiclePurchasedByOwnerEconomyTO.getPrice1());
			priceConfiguration.setPrice2Label(vehiclePurchasedByOwnerEconomyTO.getField2());
			priceConfiguration.setPrice2(vehiclePurchasedByOwnerEconomyTO.getPrice2());
			priceConfiguration.setPrice3Label(vehiclePurchasedByOwnerEconomyTO.getField3());
			priceConfiguration.setPrice3(vehiclePurchasedByOwnerEconomyTO.getPrice3());
			priceConfiguration.setDeliveryExpenses(vehiclePurchasedByOwnerEconomyTO.getDeliveryExpenses());
			priceConfiguration.setLicenseTagFee(vehiclePurchasedByOwnerEconomyTO.getLicenseTagFee());
			priceConfiguration.setCashOnDeliveryLabel(vehiclePurchasedByOwnerEconomyTO.getCashOnDeliveryLabel());
			priceConfiguration.setCashOnDelivery(vehiclePurchasedByOwnerEconomyTO.getCashOnDeliveryPrice());
			priceConfiguration.setPayment(vehiclePurchasedByOwnerEconomyTO.getPayment());
			priceConfiguration.setDiscount(priceConfigurationTO.getDiscountAmount());
			priceConfiguration.setColorCode(priceConfigurationTO.getColorCode());
			priceConfiguration.setColorText(priceConfigurationTO.getColorCodeText());
			priceConfiguration.setInteriorCode(priceConfigurationTO.getInteriorCode());
			priceConfiguration.setInteriorText(priceConfigurationTO.getInteriorCodeText());
			priceConfiguration.setCampaignCodes(priceConfigurationTO.getCampaignCodes());

			// Set the leasing offer if present
			if (null != vehiclePurchasedByOwnerTO.getLeaseOfferTO()) {
				final dk.semlerit.autocore.util.infrastructure.ws.vehiclesalecontract.v201704.to.LeaseOfferTO leaseOfferTO = vehiclePurchasedByOwnerTO
						.getLeaseOfferTO();

				final ExternalSalePacketOfferTO leasingOffer = new ExternalSalePacketOfferTO();
				leasingOffer.setId(leaseOfferTO.getId());
				leasingOffer.setProviderId(leaseOfferTO.getProviderId());
				leasingOffer.setPdf(leaseOfferTO.getPdf());

				if (null != leaseOfferTO.getLeaseValueTOs() && !leaseOfferTO.getLeaseValueTOs().isEmpty()) {
					final LinkedList<ExternalSalePacketOfferValueTO> externalSalePacketOfferValues = new LinkedList<ExternalSalePacketOfferValueTO>();
					for (final dk.semlerit.autocore.util.infrastructure.ws.vehiclesalecontract.v201704.to.LeaseValueTO leaseValueTO : leaseOfferTO
							.getLeaseValueTOs()) {
						final ExternalSalePacketOfferValueTO externalSalePacketOfferValue = new ExternalSalePacketOfferValueTO();

						externalSalePacketOfferValue.setKey(leaseValueTO.getKey());
						externalSalePacketOfferValue.setLabel(leaseValueTO.getLabel());
						externalSalePacketOfferValue.setValue(leaseValueTO.getValue());

						externalSalePacketOfferValues.add(externalSalePacketOfferValue);
					}
					leasingOffer.setExternalSalePacketOfferValues(externalSalePacketOfferValues);
				}
				priceConfiguration.setLeasingOfferTO(leasingOffer);

			}

			// Set the all inclusive offer if present
			if (null != vehiclePurchasedByOwnerTO.getAllInclusiveTO()) {
				final dk.semlerit.autocore.util.infrastructure.ws.vehiclesalecontract.v201704.to.AllInclusiveTO allInclusiveTO = vehiclePurchasedByOwnerTO
						.getAllInclusiveTO();

				final ExternalSalePacketOfferTO allInclusiveOffer = new ExternalSalePacketOfferTO();
				allInclusiveOffer.setId(allInclusiveTO.getID());
				allInclusiveOffer.setProviderId(allInclusiveTO.getProviderID());
				allInclusiveOffer.setPdf(allInclusiveTO.getPDF());

				if (null != allInclusiveTO.getAllInclusiveValueTOs()
						&& !allInclusiveTO.getAllInclusiveValueTOs().isEmpty()) {
					final LinkedList<ExternalSalePacketOfferValueTO> externalSalePacketOfferValues = new LinkedList<ExternalSalePacketOfferValueTO>();
					for (final dk.semlerit.autocore.util.infrastructure.ws.vehiclesalecontract.v201704.to.AllInclusiveValueTO allInclusiveValueTO : allInclusiveTO
							.getAllInclusiveValueTOs()) {
						final ExternalSalePacketOfferValueTO externalSalePacketOfferValue = new ExternalSalePacketOfferValueTO();

						externalSalePacketOfferValue.setKey(allInclusiveValueTO.getKey());
						externalSalePacketOfferValue.setLabel(allInclusiveValueTO.getLabel());
						externalSalePacketOfferValue.setValue(allInclusiveValueTO.getValue());

						externalSalePacketOfferValues.add(externalSalePacketOfferValue);
					}
					allInclusiveOffer.setExternalSalePacketOfferValues(externalSalePacketOfferValues);
				}
				priceConfiguration.setAllInclusiveOfferTO(allInclusiveOffer);
			}

			// Set the finance offer if present
			if (null != vehiclePurchasedByOwnerTO.getFinanceOfferTO()) {
				final dk.semlerit.autocore.util.infrastructure.ws.vehiclesalecontract.v201704.to.FinanceOfferTO financeOfferTO = vehiclePurchasedByOwnerTO
						.getFinanceOfferTO();

				final ExternalSalePacketOfferTO financeOffer = new ExternalSalePacketOfferTO();
				financeOffer.setId(financeOfferTO.getID());
				financeOffer.setProviderId(financeOfferTO.getProviderID());
				financeOffer.setPdf(financeOfferTO.getPDF());

				if (null != financeOfferTO.getFinanceValueTOs() && !financeOfferTO.getFinanceValueTOs().isEmpty()) {
					final LinkedList<ExternalSalePacketOfferValueTO> externalSalePacketOfferValues = new LinkedList<ExternalSalePacketOfferValueTO>();
					for (final dk.semlerit.autocore.util.infrastructure.ws.vehiclesalecontract.v201704.to.FinanceValueTO financeValueTO : financeOfferTO
							.getFinanceValueTOs()) {
						final ExternalSalePacketOfferValueTO externalSalePacketOfferValue = new ExternalSalePacketOfferValueTO();

						externalSalePacketOfferValue.setKey(financeValueTO.getKey());
						externalSalePacketOfferValue.setLabel(financeValueTO.getLabel());
						externalSalePacketOfferValue.setValue(financeValueTO.getValue());

						externalSalePacketOfferValues.add(externalSalePacketOfferValue);
					}
					financeOffer.setExternalSalePacketOfferValues(externalSalePacketOfferValues);
				}
				priceConfiguration.setFinanceOfferTO(financeOffer);

			}

			// Set the insurance offer if present
			if (null != vehiclePurchasedByOwnerTO.getInsuranceOfferTO()) {
				final dk.semlerit.autocore.util.infrastructure.ws.vehiclesalecontract.v201704.to.InsuranceOfferTO insuranceOfferTO = vehiclePurchasedByOwnerTO
						.getInsuranceOfferTO();

				final ExternalSalePacketOfferTO insuranceOffer = new ExternalSalePacketOfferTO();
				insuranceOffer.setId(insuranceOfferTO.getID());
				insuranceOffer.setProviderId(insuranceOfferTO.getProviderID());
				insuranceOffer.setPdf(insuranceOfferTO.getPDF());

				if (null != insuranceOfferTO.getInsuranceValueTOs()
						&& !insuranceOfferTO.getInsuranceValueTOs().isEmpty()) {
					final LinkedList<ExternalSalePacketOfferValueTO> externalSalePacketOfferValues = new LinkedList<ExternalSalePacketOfferValueTO>();
					for (final dk.semlerit.autocore.util.infrastructure.ws.vehiclesalecontract.v201704.to.InsuranceValueTO insuranceValueTO : insuranceOfferTO
							.getInsuranceValueTOs()) {
						final ExternalSalePacketOfferValueTO externalSalePacketOfferValue = new ExternalSalePacketOfferValueTO();

						externalSalePacketOfferValue.setKey(insuranceValueTO.getKey());
						externalSalePacketOfferValue.setLabel(insuranceValueTO.getLabel());
						externalSalePacketOfferValue.setValue(insuranceValueTO.getValue());

						externalSalePacketOfferValues.add(externalSalePacketOfferValue);
					}
					insuranceOffer.setExternalSalePacketOfferValues(externalSalePacketOfferValues);
				}
				priceConfiguration.setInsuranceOfferTO(insuranceOffer);
			}

			// Set the contact information if present
			if (null != vehiclePurchasedByOwnerTO.getContactInformationTO()) {

				final dk.semlerit.autocore.util.infrastructure.ws.vehiclesalecontract.v201704.to.ContactInformationTO contactInformation = vehiclePurchasedByOwnerTO
						.getContactInformationTO();

				final dk.semlerit.autocore.web.business.common.saledocument.to.ContactInformationTO contactInformationTO = new dk.semlerit.autocore.web.business.common.saledocument.to.ContactInformationTO();

				contactInformationTO.setCustomerName(contactInformation.getCustomerName());
				contactInformationTO.setDoNotUseMail(contactInformation.getDoNotUseMail());
				contactInformationTO.setDoNotUsePhone(contactInformation.getDoNotUsePhone());
				contactInformationTO.setEmail(contactInformation.getEmail());
				contactInformationTO.setUserTypeInformationSelected(
						UserType.getByCssValue(contactInformation.getUseCustomerInformationSelected()));
				contactInformationTO.setPhone(contactInformation.getPhone());
				vehiclePurchasedByCustomer.setContactInformationTO(contactInformationTO);

			}

			// Set the service contract if present
			if (null != vehiclePurchasedByOwnerTO.getServiceContractTO()) {
				final dk.semlerit.autocore.util.infrastructure.ws.vehiclesalecontract.v201704.to.ServiceContractTO serviceContract = vehiclePurchasedByOwnerTO
						.getServiceContractTO();

				final dk.semlerit.autocore.web.business.common.saledocument.to.ServiceContractTO serviceContractTO = new dk.semlerit.autocore.web.business.common.saledocument.to.ServiceContractTO();
				serviceContractTO.setPriceModelId(serviceContract.getPriceModelId());
				final dk.semlerit.autocore.util.infrastructure.ws.vehiclesalecontract.v201704.to.PaymentFrequencyTO paymentFrequency = serviceContract
						.getPaymentFrequencyTO();

				final dk.semlerit.autocore.web.business.common.saledocument.to.PaymentFrequencyTO paymentFrequencyTO = new dk.semlerit.autocore.web.business.common.saledocument.to.PaymentFrequencyTO();

				paymentFrequencyTO.setFrequency(paymentFrequency.getFrequency());
				paymentFrequencyTO.setFrequencyDescription(paymentFrequency.getFrequencyDescription());
				paymentFrequencyTO.setId(paymentFrequency.getId());
				serviceContractTO.setPaymentFrequencyTO(paymentFrequencyTO);

				serviceContractTO.setKilometersPerYear(serviceContract.getKilometersPerYear());
				serviceContractTO.setKilometersPerMonth(serviceContract.getKilometersPerMonth());
				serviceContractTO.setContractType(ContractType.getByValue(serviceContract.getContractType()));
				serviceContractTO.setContractDuration(serviceContract.getContractDuration());
				serviceContractTO.setAllowedKilometers(serviceContract.getAllowedKilometers());
				serviceContractTO.setDealerDiscountExclVAT(serviceContract.getDealerDiscountExclVAT());

				final ArrayList<dk.semlerit.autocore.web.business.common.saledocument.to.OptionTO> optionTOs = new ArrayList<dk.semlerit.autocore.web.business.common.saledocument.to.OptionTO>();
				for (final dk.semlerit.autocore.util.infrastructure.ws.vehiclesalecontract.v201704.to.OptionTO option : serviceContract
						.getOptionTOs()) {
					final dk.semlerit.autocore.web.business.common.saledocument.to.OptionTO optionTO = new dk.semlerit.autocore.web.business.common.saledocument.to.OptionTO();
					optionTO.setOptionDescription(option.getOptionDescription());
					optionTO.setOptionId(option.getOptionId());
					optionTO.setOptionPrice(option.getOptionPrice());
					optionTOs.add(optionTO);
				}
				serviceContractTO.setOptionTOs(optionTOs);

				final dk.semlerit.autocore.util.infrastructure.ws.vehiclesalecontract.v201704.to.CalculatedPriceTO basicCalculatedPrice = serviceContract
						.getBasicCalculatedPriceTO();
				final dk.semlerit.autocore.web.business.common.saledocument.to.CalculatedPriceTO basicCalculatedPriceTO = new dk.semlerit.autocore.web.business.common.saledocument.to.CalculatedPriceTO();

				basicCalculatedPriceTO
						.setAdditionalKilometersPrice(basicCalculatedPrice.getAdditionalKilometersPrice());
				basicCalculatedPriceTO.setContractPricePerKm(basicCalculatedPrice.getContractPricePerKm());
				basicCalculatedPriceTO.setPricePerPayment(basicCalculatedPrice.getPricePerPayment());
				basicCalculatedPriceTO.setTotalPrice(basicCalculatedPrice.getTotalPrice());
				basicCalculatedPriceTO.setVatIncluded(basicCalculatedPrice.isVatIncluded());
				serviceContractTO.setBasicCalculatedPriceTO(basicCalculatedPriceTO);

				priceConfiguration.setServiceContractTO(serviceContractTO);
			}

			// Set the extended warranty if present
			if (null != vehiclePurchasedByOwnerTO.getExtendedWarrantyTO()) {
				final dk.semlerit.autocore.util.infrastructure.ws.vehiclesalecontract.v201704.to.ExtendedWarrantyTO extendedWarrantyTO = vehiclePurchasedByOwnerTO
						.getExtendedWarrantyTO();

				final ExtendedWarrantyTO extendedWarranty = new ExtendedWarrantyTO();
				extendedWarranty.setId(extendedWarrantyTO.getId());
				extendedWarranty.setDescription(extendedWarrantyTO.getDescription());
				extendedWarranty.setPriceIncludingVAT(extendedWarrantyTO.getPriceIncludingVAT());
				extendedWarranty.setPriceExcludingVAT(extendedWarrantyTO.getPriceExcludingVAT());
				extendedWarranty.setVAT(extendedWarrantyTO.getVAT());
				priceConfiguration.setExtendedWarrantyTO(extendedWarranty);
			}
			document.setPriceConfigurationTO(priceConfiguration);
		}

		document.setOwner1TO(getCustomerTOfromSaleContract(saleContractTO.getOwner1TO(), remoteUser));
		document.setOwner2TO(getCustomerTOfromSaleContract(saleContractTO.getOwner2TO(), remoteUser));
		document.setUser1TO(getCustomerTOfromSaleContract(saleContractTO.getUser1TO(), remoteUser));
		document.setUser2TO(getCustomerTOfromSaleContract(saleContractTO.getUser2TO(), remoteUser));

		if (null != saleContractTO.getDealerTO()) {
			final dk.semlerit.autocore.util.infrastructure.ws.vehiclesalecontract.v201704.to.DealerTO dealerTO = saleContractTO
					.getDealerTO();

			final DealerTO dealer = new DealerTO();
			dealer.setName(dealerTO.getName());
			dealer.setAddress(dealerTO.getAddress());
			dealer.setZip(dealerTO.getZip());
			dealer.setCity(dealerTO.getCity());
			dealer.setPhoneNumber(dealerTO.getPhoneNumber());
			dealer.setFaxNumber(dealerTO.getFaxNumber());
			dealer.setBankRegistrationNumber(dealerTO.getBankRegistrationNumber());
			dealer.setBankAccountNumber(dealerTO.getBankAccountNumber());
			dealer.setSwift(dealerTO.getSwift());
			dealer.setIban(dealerTO.getIban());
			dealer.setCvrNumber(dealerTO.getCvrNumber());
			dealer.setEmail(dealerTO.getEmail());

			document.setDealerTO(dealer);
		}
		final dk.semlerit.autocore.util.infrastructure.ws.vehiclesalecontract.v201704.to.VehicleSoldToDealerTO vehicleSoldToDealerTO = saleContractTO
				.getVehicleSoldToDealerTO();
		if (null != vehicleSoldToDealerTO && (null != vehicleSoldToDealerTO.getMake()
				&& StringUtils.isNotEmpty(vehicleSoldToDealerTO.getModel()))) {
			final PriceConfigurationTO priceConfiguration = document.getPriceConfigurationTO();
			final EconomyTO economyTO = document.getEconomyTO();

			final VehicleSoldToDealerTO vehicleSoldToDealer = new VehicleSoldToDealerTO();
			vehicleSoldToDealer.setMake(vehicleSoldToDealerTO.getMake());
			vehicleSoldToDealer.setModel(vehicleSoldToDealerTO.getModel());
			vehicleSoldToDealer.setIdentificationNumber(vehicleSoldToDealerTO.getIdentificationNumber());
			vehicleSoldToDealer.setFirstRegistrationDate(vehicleSoldToDealerTO.getFirstRegistrationDate());
			vehicleSoldToDealer.setModelYear(vehicleSoldToDealerTO.getModelYear());
			vehicleSoldToDealer.setProductionYear(vehicleSoldToDealerTO.getProductionYear());
			vehicleSoldToDealer.setColor(vehicleSoldToDealerTO.getColor());
			vehicleSoldToDealer.setLicensePlate(vehicleSoldToDealerTO.getLicensePlate());
			vehicleSoldToDealer.setEquipment(vehicleSoldToDealerTO.getEquipment());
			vehicleSoldToDealer.setExtraEquipment(vehicleSoldToDealerTO.getExtraEquipment());
			vehicleSoldToDealer.setInspectionDate(vehicleSoldToDealerTO.getInspectionDate());
			vehicleSoldToDealer.setKilometers(vehicleSoldToDealerTO.getKilometers());
			vehicleSoldToDealer.setTimingBeltChangeDate(vehicleSoldToDealerTO.getTimingBeltChangeDate());
			vehicleSoldToDealer.setTimingBeltChangeKilometers(vehicleSoldToDealerTO.getTimingBeltChangeKilometers());
			vehicleSoldToDealer.setCorrosionWarranty(vehicleSoldToDealerTO.getCorrosionWarranty());
			vehicleSoldToDealer.setRegistrationTaxRule(vehicleSoldToDealerTO.getRegistrationTaxRule());
			vehicleSoldToDealer.setUsage(vehicleSoldToDealerTO.getUsage());
			vehicleSoldToDealer.setInvolvedInAccident(vehicleSoldToDealerTO.getInvolvedInAccident());
			vehicleSoldToDealer.setAccidentDescription(vehicleSoldToDealerTO.getAccidentDescription());
			vehicleSoldToDealer.setAdditionalComments(vehicleSoldToDealerTO.getAdditionalComments());
			priceConfiguration.setVehicleSoldToDealerPrice(vehicleSoldToDealerTO.getPriceExcludingVat());
			priceConfiguration.setVehicleSoldToDealerVat(vehicleSoldToDealerTO.getVat());
			economyTO.setVehicleSoldToDealerPrice(vehicleSoldToDealerTO.getTotalprice());
			priceConfiguration.setVehicleSoldToDealerOutstandingDebt(vehicleSoldToDealerTO.getOutstandingDebt());
			vehicleSoldToDealer.setOutstandingDebtTo(vehicleSoldToDealerTO.getOutstandingDebtTo());
			vehicleSoldToDealer.setDamagePaidBy(vehicleSoldToDealerTO.getDamagePaidBy());
			vehicleSoldToDealer.setDamageScope(vehicleSoldToDealerTO.getDamageScope());
			vehicleSoldToDealer.setDamageInstallmentAmount(vehicleSoldToDealerTO.getDamageInstallmentAmount());
			vehicleSoldToDealer.setServiceBookMaintained(vehicleSoldToDealerTO.getServiceBookMaintained());

			if (null != vehicleSoldToDealerTO.getSrVehicleTradeTO()) {
				final SRVehicleTradeTO srVehicleTradeTO = vehicleSoldToDealerTO.getSrVehicleTradeTO();

				final SemlerRetailVehicleTradeTO srVehicleTrade = new SemlerRetailVehicleTradeTO();
				srVehicleTrade.setTradeType(SRTradeType.getByValue(srVehicleTradeTO.getTradeType()));
				srVehicleTrade
						.setDepartmentNumber(SRDepartmentNumberType.getByValue(srVehicleTradeTO.getDepartmentNumber()));
				srVehicleTrade.setSellingPrice(srVehicleTradeTO.getSellingPrice());
				srVehicleTrade.setEmployeeNumber(srVehicleTradeTO.getEmployeeNumber());
				srVehicleTrade.setFee(srVehicleTradeTO.getFee());
				vehicleSoldToDealer.setSrVehicleTradeTO(srVehicleTrade);
			}

			document.setVehicleSoldToDealerTO(vehicleSoldToDealer);
		}

		if (null != saleContractTO.getHistoryLogTO() && !saleContractTO.getHistoryLogTO().isEmpty()) {
			final ArrayList<HistoryLogTO> historyLogs = new ArrayList<HistoryLogTO>();
			for (final dk.semlerit.autocore.util.infrastructure.ws.vehiclesalecontract.v201704.to.HistoryLogTO historyLogTO : saleContractTO
					.getHistoryLogTO()) {
				final HistoryLogTO historyLog = new HistoryLogTO();

				historyLog.setChanged(historyLogTO.getChanged());
				historyLog.setBy(historyLogTO.getBy());
				historyLog.setDocumentType(DocumentType.getByCode(historyLogTO.getDocumentType()));
				historyLog.setStatus(historyLogTO.getStatus());

				historyLogs.add(historyLog);
			}
			document.setHistoryLogTOs(historyLogs);
		}

		if (null != saleContractTO.getEmailLogTO() && !saleContractTO.getEmailLogTO().isEmpty()) {
			final ArrayList<EmailLogTO> emailLogs = new ArrayList<EmailLogTO>();
			for (final dk.semlerit.autocore.util.infrastructure.ws.vehiclesalecontract.v201704.to.EmailLogTO emailLogTO : saleContractTO
					.getEmailLogTO()) {
				final EmailLogTO emailLog = new EmailLogTO();

				emailLog.setDate(emailLogTO.getDate());
				emailLog.setEmail(emailLogTO.getEmail());
				emailLog.setContent(emailLogTO.getContent());
				emailLog.setTopic(emailLogTO.getTopic());

				emailLogs.add(emailLog);
			}
			document.setEmailLogTOs(emailLogs);
		}

		if (null != saleContractTO.getVersionLogTO() && !saleContractTO.getVersionLogTO().isEmpty()) {
			final ArrayList<VersionLogTO> versionLogTOs = new ArrayList<VersionLogTO>();
			for (final dk.semlerit.autocore.util.infrastructure.ws.vehiclesalecontract.v201704.to.VersionLogTO versionLogTO : saleContractTO
					.getVersionLogTO()) {
				final VersionLogTO versionLog = new VersionLogTO();
				versionLog.setDocumentID(versionLogTO.getDocumentID());
				versionLog.setVersion(versionLogTO.getVersion());
				versionLog.setCreateUser(versionLogTO.getCreateUser());
				versionLog.setCreateDate(versionLogTO.getCreateDate());
				versionLog.setChangeUser(versionLogTO.getChangeUser());
				versionLog.setChangeDate(versionLogTO.getChangeDate());

				versionLogTOs.add(versionLog);
			}
			document.setVersionLogTOs(versionLogTOs);
		}

		document.setCrmTO(saleContractTO.getCRMTO());
		document.setVersion(saleContractTO.getVersion());

		return document;
	}catch(

	final Exception e)
	{
		throw new ACPortletException(remoteUser, "Could not create a document from sale contract service retrieval.",
				ACConstants.PROJECT_SALESPORTALWEB, CLASS_NAME, methodName, saleContractTO, e);
	}
	}

	private static DocumentCustomerTO getCustomerTOfromSaleContract(
			final dk.semlerit.autocore.util.infrastructure.ws.vehiclesalecontract.v201704.to.CustomerTO saleContractCustomerTO,
			final String remoteUser) {
		final String methodName = "getCustomerTOfromSaleContract";

		try {
			DocumentCustomerTO customerTO = null;
			if (saleContractCustomerTO != null) {
				customerTO = new DocumentCustomerTO();
				customerTO.setCustomerId(saleContractCustomerTO.getCustomerId());
				customerTO.setAddress1(saleContractCustomerTO.getAddress());
				customerTO.setCoName(saleContractCustomerTO.getAttention());
				customerTO.setTown(saleContractCustomerTO.getCity());
				customerTO.setZipCode(saleContractCustomerTO.getZip());
				customerTO.setMailAddress(saleContractCustomerTO.getCustomerEmail());
				customerTO.setFirstName(saleContractCustomerTO.getFirstName());
				customerTO.setLastName(saleContractCustomerTO.getLastName());
				customerTO.setExtraInformation(saleContractCustomerTO.getExtraInformation());
				customerTO.setIdentificationIndicator(saleContractCustomerTO.getIdentificationIndicator());
				customerTO.setCvrCprIndicator(
						CvrCprIndicatorType.getByIndicator(customerTO.getIdentificationIndicator()));
				customerTO.setSocialSecurityOrCVRNumber(saleContractCustomerTO.getSocialSecurityOrCVRNumber());
				customerTO.setMobileNumber(saleContractCustomerTO.getMobileNumber());
				customerTO.setPhoneNumber(saleContractCustomerTO.getPhoneNumber());
				customerTO.setCustomerType(saleContractCustomerTO.getCustomerType());
				customerTO.setCountry(saleContractCustomerTO.getCountry());
			}

			return customerTO;
		} catch (final Exception e) {
			throw new ACSystemException(remoteUser, "Could not get customer", ACConstants.PROJECT_INFRASTRUCTUREUTIL,
					CLASS_NAME, methodName, saleContractCustomerTO, e);
		}
	}

	/**
	 * <p>
	 * This method is meant for migrating older documents containing deprecated
	 * delivery expense type values. Deprecated in that sense, that UK and THKI have
	 * set new requirements for how delivery expenses is handled.
	 * </p>
	 * 
	 * Rules are:
	 * <ul>
	 * <li><code>null</code> is migrated to type -1 - NOT_SPECIFIED</li>
	 * <li>Type 10 is migrated to type 0 - The old CAR_TYPE_COACH_INCL_VAT is
	 * migrated to COACH</li>
	 * <li>Type 9 is migrated to type 3 - The old CAR_TYPE_VAN_INCL_VAT is migrated
	 * to VAN_INCL_VAT_30</li>
	 * <li>Type 8 is migrated to type 1 - The old CAR_TYPE_VAN_EXCL_VAT is migrated
	 * to VAN_EXCL_VAT_30</li>
	 * </ul>
	 * 
	 * @param type
	 * @return a migrated delivery expense type code or <code>NOT_SPECIFIED</code>
	 *         if type parameter is null.
	 */
	private static Integer doMigrationCheck(final Integer type) {
		if (null == type) {
			return -1; // Not Specified
		}
		if (type.intValue() == 10) {
			return 0;
		} else if (type.intValue() == 9) {
			return 3;
		} else if (type.intValue() == 8) {
			return 1;
		} else {
			return new Integer(type);
		}
	}

	/**
	 * returns a null safe get of UsedVehicleTypeIndicator by either a direct
	 * resolve using enum value. or in case that the value is null for older non
	 * converted documents it tries to resolve it by delivery expense type.
	 * 
	 * VAN_EXCL_VAT_30 => VAN VAN_INCL_VAT_30 => VAN <ALL OTHER> => CAR/COACH
	 * 
	 * @param usedVehicleTypeIndicator
	 *            , value from service may be null for older non converted documents
	 * @return UsedVehicleIndicatorType either resolved by type or deliveryExpense
	 *         type.
	 */
	private static UsedVehicleTypeIndicatorType resolveUsedVehicleTypeIndicator(final Integer usedVehicleTypeIndicator,
			final PriceConfigurationTO priceConfigurationTO) {
		if (null != usedVehicleTypeIndicator) {
			return UsedVehicleTypeIndicatorType.getByValue(usedVehicleTypeIndicator);
		}

		switch (priceConfigurationTO.getDeliveryExpenseTypeCode()) {
		case VAN_EXCL_VAT_30:
			return UsedVehicleTypeIndicatorType.VAN;
		case VAN_INCL_VAT_30:
			return UsedVehicleTypeIndicatorType.VAN;
		default:
			return UsedVehicleTypeIndicatorType.COACH;
		}
	}

	/**
	 * Populate document with vehicle data
	 * 
	 * @param documentTO
	 * @param modelVariantTO
	 * @return
	 */
	public static DocumentTO populateDocumentTOWithVehicleData(final DocumentTO documentTO,
			final ModelVariantTO modelVariantTO) {
		final VehiclePurchasedByCustomerTO vehiclePurchasedByCustomerTO = documentTO.getVehiclePurchasedByCustomerTO();
		final PriceConfigurationTO priceConfigurationTO = documentTO.getPriceConfigurationTO();

		if (null != modelVariantTO) {
			vehiclePurchasedByCustomerTO.setTypeCode(modelVariantTO.getTypeCode());
			vehiclePurchasedByCustomerTO.setModelYear(modelVariantTO.getYear());
			vehiclePurchasedByCustomerTO.setVariant(modelVariantTO.getVariant());
			vehiclePurchasedByCustomerTO.setVanTaxRate(modelVariantTO.getVanTaxRate());
			vehiclePurchasedByCustomerTO.setPrNumbers(modelVariantTO.getPrNumber());
			vehiclePurchasedByCustomerTO.setHorsePower(modelVariantTO.getHorsePower());

			// Set price code if not already set
			if (StringUtils.isEmpty(priceConfigurationTO.getPriceCode())) {
				priceConfigurationTO.setPriceCode(modelVariantTO.getDefaultPriceCode());
			}

			if (modelVariantTO.getVanTaxRate() == 1) { // 0%
				priceConfigurationTO.setTaxIndicator(TaxIndicatorType.EXCLUSIVE_TAX);
			}

			final StringBuilder modelType = new StringBuilder(modelVariantTO.getTypeCode());
			if (modelVariantTO.getFuelType() != null) {
				switch (modelVariantTO.getFuelType()) {
				case 1:
					modelType.append("-Benzin");
					vehiclePurchasedByCustomerTO.setFuelType("Benzin");
					break;
				case 2:
					modelType.append("-Diesel");
					vehiclePurchasedByCustomerTO.setFuelType("Diesel");
					break;
				case 3:
					modelType.append("-El");
					vehiclePurchasedByCustomerTO.setFuelType("El");
					break;
				}
			}
			modelType.append('-').append(modelVariantTO.getHorsePower()).append(" hk");
			modelType.append('-').append(modelVariantTO.getTransmission());
			modelType.append('-').append(modelVariantTO.getDoors()).append("-d�rs");
			vehiclePurchasedByCustomerTO.setVehicleType(modelType.toString());
		}
		final StringBuilder modelText = new StringBuilder();

		if (StringUtils.isNotEmpty(vehiclePurchasedByCustomerTO.getUniqueModelId())) {
			switch (vehiclePurchasedByCustomerTO.getMake()) {
			case AUDI:
				modelText.append("Audi");
				break;
			case SEAT:
				modelText.append("SEAT");
				break;
			case SKODA:
				modelText.append("Skoda");
				break;
			case VW:
			case VWVAN:
				modelText.append("Volkswagen");
				break;
			case PORSCHE:
				modelText.append("Porsche");
				break;
			default:
			}
			modelText.append(' ').append(vehiclePurchasedByCustomerTO.getUniqueModelId());
			modelText.append(' ').append(modelVariantTO.getVariant());
			modelText.append(' ').append(modelVariantTO.getEngine());
			modelText.append(' ').append(modelVariantTO.getTransmission());
			vehiclePurchasedByCustomerTO.setModel(vehiclePurchasedByCustomerTO.getUniqueModelId());
			vehiclePurchasedByCustomerTO.setModelText(modelText.toString());
		}
		return documentTO;
	}

	/**
	 * Populate document with vehicle data
	 * 
	 * @param documentTO
	 * @param vehicleTO
	 * @param documentEconomyViewTO
	 * @return
	 */
	public static DocumentTO populateDocumentTOWithVehicleData(final String remoteUser, final DocumentTO documentTO,
			final VehicleTO vehicleTO) {
		final VehiclePurchasedByCustomerTO vehiclePurchasedByCustomerTO = documentTO.getVehiclePurchasedByCustomerTO();
		final PriceConfigurationTO priceConfigurationTO = documentTO.getPriceConfigurationTO();
		if (vehicleTO != null) {
			if (StringUtils.isNumeric(vehicleTO.getModelYearYYYY())) {
				vehiclePurchasedByCustomerTO.setModelYear(Integer.parseInt(vehicleTO.getModelYearYYYY()));
			}

			vehiclePurchasedByCustomerTO.setVehicleType(vehicleTO.getVehicleType());
			vehiclePurchasedByCustomerTO.setScheduleCode(vehicleTO.getScheduleCode());
			vehiclePurchasedByCustomerTO.setVin(vehicleTO.getVin());
			vehiclePurchasedByCustomerTO.setVehicleId(vehicleTO.getVehicleId());
			vehiclePurchasedByCustomerTO.setVehiclePrItemTOs(new ArrayList<PrItemTO>(vehicleTO.getPrItems()));
			vehiclePurchasedByCustomerTO.setBrandCode(vehicleTO.getBrandCode());
			vehiclePurchasedByCustomerTO.setFuelType(vehicleTO.getFuelType());
			vehiclePurchasedByCustomerTO.setTypeCode(vehicleTO.getVehicleType());
			vehiclePurchasedByCustomerTO.setStatusCode(vehicleTO.getStatusCode());

			if (null != vehicleTO.getRegistrationTO()) {
				final RegistrationTO registrationTO = vehicleTO.getRegistrationTO();
				if (null != registrationTO.getLicenseDate()) {
					vehiclePurchasedByCustomerTO.setFirstRegistrationDate(
							DateAndTimeUtil.DEFAULT_DATE_FORMAT.format(registrationTO.getLicenseDate()));
				}

				if (VehicleMakeType
						.isNewVehicleMakeStockIncluded(documentTO.getVehiclePurchasedByCustomerTO().getMake())
						|| StringUtils.isBlank(documentTO.getId())) {
					vehiclePurchasedByCustomerTO.setLicensePlate(registrationTO.getLicenseTags());
				}
			}
			if (StringUtils.isEmpty(vehiclePurchasedByCustomerTO.getKilometers()) && vehicleTO.getHistoryTO() != null
					&& vehicleTO.getHistoryTO().getOdometer() != null) {
				vehiclePurchasedByCustomerTO.setKilometers(String.valueOf(vehicleTO.getHistoryTO().getOdometer()));
			}

			// if there is a mileage registered on the vehicle, use that instead
			// of the value registered on the vehicle type
			if (null != vehicleTO.getKlmPrLtr()) {
				vehiclePurchasedByCustomerTO.setKilometersPerLiter(
						new BigDecimal(vehicleTO.getKlmPrLtr()).setScale(2, RoundingMode.HALF_UP));
			}

			// use price code from the vehicleTO if it is NEW_OWN_BRAND
			if (vehiclePurchasedByCustomerTO.getMake() == VehicleMakeType.NEW_OWN_BRAND) {
				priceConfigurationTO.setPriceCode(vehicleTO.getPriceCode());
			}

		}
		return documentTO;
	}

	/**
	 * Fills in the given OrderTO with values from the given DocumentTO. The orderTO
	 * object can be pre-populated to make delta updates easier.
	 * 
	 * @param request
	 * @param documentTO
	 * @param orderTO
	 * @param remoteUser
	 *            only used for filling in in exceptions where needed.
	 * @throws ACPortletException
	 * @throws ACSystemException
	 * @return the given OrderTO is returned for convenience, making chained calls
	 *         possible.
	 * @throws ParseException
	 *             if the FirstRegistrationDate, DeliveryDate or InspectionDate
	 *             cannot be parsed.
	 */
	public static OrderTO mapToOrderTO(final PortletRequest request, final DocumentTO documentTO, final OrderTO orderTO,
			final String remoteUser) throws ParseException {
		orderTO.setTaxFree(documentTO.getPriceConfigurationTO().getVatIndicator() == VatIndicatorType.EXCLUSIVE_VAT);
		if (documentTO.getVehiclePurchasedByCustomerTO() != null) {
			if (orderTO.getVehiclePurchasedByCustomerTO() == null) {
				orderTO.setVehiclePurchasedByCustomerTO(
						new dk.semlerit.autocore.util.infrastructure.ws.salesorder.v201310.to.VehiclePurchasedByCustomerTO());
			}

			// Due to some differences in how the service retrieve and update
			// methods are designed and how the delta copy works, we always need
			// an initialized ExtendedGuaranteeTO.
			orderTO.getVehiclePurchasedByCustomerTO().setExtendedGuaranteeTO(new ExtendedGuaranteeTO());
			if (documentTO.getPriceConfigurationTO() != null
					&& documentTO.getPriceConfigurationTO().getExtendedWarrantyTO() != null) {
				orderTO.getVehiclePurchasedByCustomerTO().getExtendedGuaranteeTO()
						.setId(documentTO.getPriceConfigurationTO().getExtendedWarrantyTO().getId());
			}

			orderTO.getVehiclePurchasedByCustomerTO().setLicenseTag(
					StringUtils.left(documentTO.getVehiclePurchasedByCustomerTO().getLicensePlate(), 10));
			orderTO.getVehiclePurchasedByCustomerTO()
					.setLicensePlateFee(documentTO.getPriceConfigurationTO().getLicenseTagFee());
			orderTO.getVehiclePurchasedByCustomerTO()
					.setDeliveryCost(documentTO.getPriceConfigurationTO().getDeliveryExpenses());

			if (StringUtils.isNotBlank(documentTO.getVehiclePurchasedByCustomerTO().getKilometers())) {
				orderTO.getVehiclePurchasedByCustomerTO()
						.setOdometer(Integer.parseInt(documentTO.getVehiclePurchasedByCustomerTO().getKilometers()));
			}

			if (StringUtils.isNotBlank(documentTO.getVehiclePurchasedByCustomerTO().getFirstRegistrationDate())) {
				orderTO.getVehiclePurchasedByCustomerTO().setFirstLicenseDate(DateAndTimeUtil
						.parseDate(documentTO.getVehiclePurchasedByCustomerTO().getFirstRegistrationDate()));
			}
			if (StringUtils.isNotBlank(documentTO.getVehiclePurchasedByCustomerTO().getProductionYear())) {
				orderTO.getVehiclePurchasedByCustomerTO().setProductionYear(
						NumberUtils.createInteger(documentTO.getVehiclePurchasedByCustomerTO().getProductionYear()));
			}

			if (VehicleMakeType.isNewVehicleMakeStockIncluded(documentTO.getVehiclePurchasedByCustomerTO().getMake())) {
				orderTO.getVehiclePurchasedByCustomerTO()
						.setEqTaxFree(documentTO.getEconomyTO().getEquipmentProvidedByDealerTotalPrice());
				// only set when <> null to please the delta copy
				orderTO.getVehiclePurchasedByCustomerTO()
						.setDiscountDutyFree(NumberUtil.nullIfZero(documentTO.getPriceConfigurationTO().getDiscount()));
			} else {
				orderTO.getVehiclePurchasedByCustomerTO()
						.setModelPrice(NumberUtil.nullIfZero(documentTO.getPriceConfigurationTO().getSalesPrice()));
				orderTO.getVehiclePurchasedByCustomerTO().setEqPrice(
						NumberUtil.nullIfZero(documentTO.getEconomyTO().getEquipmentProvidedByDealerTotalPrice()));
			}
			orderTO.setDownPayment((NumberUtil.nullIfZero(documentTO.getPriceConfigurationTO().getPayment())));

			if (StringUtils.isNotBlank(documentTO.getVehiclePurchasedByCustomerTO().getDeliveryWeek())
					&& StringUtils.isNotBlank(documentTO.getVehiclePurchasedByCustomerTO().getDeliveryYear())) {
				AgreedDeliveryYearWeekTO agreedDeliveryYearWeekTO = orderTO.getVehiclePurchasedByCustomerTO()
						.getAgreedDeliveryYearWeekTO();
				if (null == agreedDeliveryYearWeekTO) {
					agreedDeliveryYearWeekTO = new AgreedDeliveryYearWeekTO();
					orderTO.getVehiclePurchasedByCustomerTO().setAgreedDeliveryYearWeekTO(agreedDeliveryYearWeekTO);
				}

				agreedDeliveryYearWeekTO.setAgreedDeliveryWeek(
						Integer.parseInt(documentTO.getVehiclePurchasedByCustomerTO().getDeliveryWeek()));
				agreedDeliveryYearWeekTO.setAgreedDeliveryYear(
						Integer.parseInt(documentTO.getVehiclePurchasedByCustomerTO().getDeliveryYear()));
			}
		}
		if (documentTO.getVehicleSoldToDealerTO() != null) {
			dk.semlerit.autocore.util.infrastructure.ws.salesorder.v201310.to.VehicleSoldToDealerTO vehicleSoldToDealerOrderTO = orderTO
					.getVehicleSoldToDealerTO();

			if (orderTO.getVehicleSoldToDealerTO() == null) {
				vehicleSoldToDealerOrderTO = new dk.semlerit.autocore.util.infrastructure.ws.salesorder.v201310.to.VehicleSoldToDealerTO();
				orderTO.setVehicleSoldToDealerTO(vehicleSoldToDealerOrderTO);
			}

			final VehicleSoldToDealerTO vehicleSoldToDealerDocumentTO = documentTO.getVehicleSoldToDealerTO();

			final TradeoffCarPurchasedFromTO tradeoffCarPurchasedFromTO = new TradeoffCarPurchasedFromTO();
			tradeoffCarPurchasedFromTO
					.setName(documentTO.getOwner1TO().getFirstName() + " " + documentTO.getOwner1TO().getLastName());
			tradeoffCarPurchasedFromTO.setAddress(documentTO.getOwner1TO().getAddress1());
			tradeoffCarPurchasedFromTO.setTown(documentTO.getOwner1TO().getTown());
			tradeoffCarPurchasedFromTO.setZipCode(documentTO.getOwner1TO().getZipCode());
			vehicleSoldToDealerOrderTO.setTradeoffCarPurchasedFromTO(tradeoffCarPurchasedFromTO);

			vehicleSoldToDealerOrderTO.setModelText(vehicleSoldToDealerDocumentTO.getModel());
			vehicleSoldToDealerOrderTO.setVin(vehicleSoldToDealerDocumentTO.getIdentificationNumber());
			vehicleSoldToDealerOrderTO
					.setLicenseTag(StringUtils.left(vehicleSoldToDealerDocumentTO.getLicensePlate(), 10));
			if (vehicleSoldToDealerDocumentTO.getMake() != null
					&& ACBrandConstants.BRANDS.containsKey(vehicleSoldToDealerDocumentTO.getMake().longValue())) {
				vehicleSoldToDealerOrderTO
						.setBrand(ACBrandConstants.BRANDS.get(vehicleSoldToDealerDocumentTO.getMake().longValue())[0]);
			}

			if (StringUtils.isNotBlank(vehicleSoldToDealerDocumentTO.getKilometers())) {
				vehicleSoldToDealerOrderTO.setOdometer(Integer.parseInt(vehicleSoldToDealerDocumentTO.getKilometers()));
			}
			if (vehicleSoldToDealerDocumentTO.getUsage() != null) {
				final int usageAlias = vehicleSoldToDealerDocumentTO.getUsage();
				String usage = null;
				if (usageAlias == VehicleSaleContract201704Util.USAGE_PRIVATE) {
					usage = VehicleSaleContract201704Util.USAGE_PRIVATE_STRING;
				} else if (usageAlias == VehicleSaleContract201704Util.USAGE_SCHOOLCAR) {
					usage = VehicleSaleContract201704Util.USAGE_SCHOOLCAR_STRING;
				} else if (usageAlias == VehicleSaleContract201704Util.USAGE_BUSINESS) {
					usage = VehicleSaleContract201704Util.USAGE_BUSINESS_STRING;
				}
				vehicleSoldToDealerOrderTO.setFormerUse(usage);
			}
			vehicleSoldToDealerOrderTO.setLastInspectionDate(
					DateAndTimeUtil.parseDate(vehicleSoldToDealerDocumentTO.getInspectionDate()));
			vehicleSoldToDealerOrderTO.setPurchasePriceTO(new PurchasePriceTO());
			vehicleSoldToDealerOrderTO.getPurchasePriceTO()
					.setPriceExclVat(documentTO.getPriceConfigurationTO().getVehicleSoldToDealerPrice());
			vehicleSoldToDealerOrderTO.getPurchasePriceTO().setOutstandingDebt(NumberUtil
					.nullIfZero(documentTO.getPriceConfigurationTO().getVehicleSoldToDealerOutstandingDebt()));
			if (documentTO.getPriceConfigurationTO().getVehicleSoldToDealerPrice() == null) {
				vehicleSoldToDealerOrderTO.getPurchasePriceTO().setVat(
						NumberUtil.nullIfZero(documentTO.getPriceConfigurationTO().getVehicleSoldToDealerVat()));
			} else {
				// Must be zero if vat is blank and price is specified
				vehicleSoldToDealerOrderTO.getPurchasePriceTO().setVat(
						NumberUtil.zeroIfNull(documentTO.getPriceConfigurationTO().getVehicleSoldToDealerVat()));
			}
			vehicleSoldToDealerOrderTO.setColorText(StringUtils.left(vehicleSoldToDealerDocumentTO.getColor(), 40));

			final StringBuffer remarks = new StringBuffer();
			if (StringUtils.isNotBlank(vehicleSoldToDealerDocumentTO.getExtraEquipment())) {
				remarks.append(vehicleSoldToDealerDocumentTO.getExtraEquipment()).append(",");
			}
			if (StringUtils.isNotBlank(vehicleSoldToDealerDocumentTO.getAccidentDescription())) {
				remarks.append(vehicleSoldToDealerDocumentTO.getAccidentDescription()).append(",");
			}
			if (StringUtils.isNotBlank(vehicleSoldToDealerDocumentTO.getAccidentDescription())) {
				remarks.append(vehicleSoldToDealerDocumentTO.getAdditionalComments());
			}
			vehicleSoldToDealerOrderTO.setRemarks1(remarks.toString());

			vehicleSoldToDealerOrderTO.setModelYear(Integer.parseInt(vehicleSoldToDealerDocumentTO.getModelYear()));

			if (StringUtils.isNotBlank(vehicleSoldToDealerDocumentTO.getProductionYear())) {
				vehicleSoldToDealerOrderTO.setProductionYear(
						NumberUtils.createInteger(vehicleSoldToDealerDocumentTO.getProductionYear()));
			}
			vehicleSoldToDealerOrderTO.setArrivalDate(
					DateAndTimeUtil.parseDate(documentTO.getVehiclePurchasedByCustomerTO().getDeliveryDate()));
			if (vehicleSoldToDealerDocumentTO.getFirstRegistrationDate() != null) {
				vehicleSoldToDealerOrderTO.setLicenseDate(
						DateAndTimeUtil.parseDate(vehicleSoldToDealerDocumentTO.getFirstRegistrationDate()));
			}
		}
		if (documentTO.getOwner1TO() != null && StringUtils.isNotBlank(documentTO.getOwner1TO().getCustomerId())) {
			if (orderTO.getCustomerTO() == null) {
				orderTO.setCustomerTO(
						new dk.semlerit.autocore.util.infrastructure.ws.salesorder.v201310.to.CustomerTO());
			}
			orderTO.getCustomerTO().setId(Integer.parseInt(documentTO.getOwner1TO().getCustomerId()));
			orderTO.getCustomerTO().setName(documentTO.getOwner1TO().getFullName());
			orderTO.getCustomerTO().setAddress((documentTO.getOwner1TO().getAddress1()));
			orderTO.getCustomerTO().setCoName(documentTO.getOwner1TO().getCoName());
			orderTO.getCustomerTO().setCountryCode(documentTO.getOwner1TO().getCountry());
			orderTO.getCustomerTO().setZipCode(documentTO.getOwner1TO().getZipCode());
			orderTO.getCustomerTO().setTown(documentTO.getOwner1TO().getTown());
			orderTO.getCustomerTO().setTypeCode(documentTO.getOwner1TO().getCustomerTopic());
			final List<String> telephoneList = new ArrayList<String>();
			if (StringUtils.isNotBlank(documentTO.getOwner1TO().getPhoneNumber())) {
				telephoneList.add(documentTO.getOwner1TO().getPhoneNumber());
			}
			if (StringUtils.isNotBlank(documentTO.getOwner1TO().getMobileNumber())) {
				telephoneList.add(documentTO.getOwner1TO().getMobileNumber());
			}
			orderTO.getCustomerTO().setTelephoneList(telephoneList);

			if (documentTO.getUser1TO() != null && StringUtils.isNotBlank(documentTO.getUser1TO().getCustomerId())) {
				if (orderTO.getVehiclePurchasedByCustomerTO().getVehicleUserTO() == null) {
					orderTO.getVehiclePurchasedByCustomerTO().setVehicleUserTO(
							new dk.semlerit.autocore.util.infrastructure.ws.salesorder.v201310.to.VehicleUserTO());
				}
				orderTO.getVehiclePurchasedByCustomerTO().getVehicleUserTO()
						.setId(Integer.parseInt(documentTO.getUser1TO().getCustomerId()));
				orderTO.getVehiclePurchasedByCustomerTO().getVehicleUserTO()
						.setName(documentTO.getUser1TO().getFullName());
				orderTO.getVehiclePurchasedByCustomerTO().getVehicleUserTO()
						.setCountryCode(documentTO.getUser1TO().getCountry());
				orderTO.getVehiclePurchasedByCustomerTO().getVehicleUserTO()
						.setZipCode(documentTO.getUser1TO().getZipCode());
				orderTO.getVehiclePurchasedByCustomerTO().getVehicleUserTO().setTown(documentTO.getUser1TO().getTown());
			} else {
				orderTO.getVehiclePurchasedByCustomerTO().setVehicleUserTO(null);
			}
		}
		return orderTO;
	}
}
