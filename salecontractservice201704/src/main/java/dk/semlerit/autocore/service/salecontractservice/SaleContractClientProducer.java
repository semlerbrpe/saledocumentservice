package dk.semlerit.autocore.service.salecontractservice;

import java.net.URI;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;
import javax.xml.ws.BindingProvider;

import dk.semlerit.autocore.ws.ServiceClientProducer;
import dk.semlerit.autocore.ws.annotation.BaseURI;
import dk.semlerit.autocore.ws.vehiclesalecontract201704.client.VehicleSaleContract;
import dk.semlerit.autocore.ws.vehiclesalecontract201704.client.VehicleSaleContract201704;

/**
 * 
 * 
 * @author edbbrpe
 * @version 1.0
 * @since 3.0.0
 */
@ApplicationScoped
class SaleContractClientProducer implements ServiceClientProducer<VehicleSaleContract> {
	private static final String ESB_SERVICE_ENDPOINT = "ws/201704/VehicleSaleContract";

	@Produces
	@Override
	public VehicleSaleContract produce(@BaseURI String baseURI) {
		final VehicleSaleContract201704 vehicleSaleContract201704 = new VehicleSaleContract201704();
		VehicleSaleContract port = vehicleSaleContract201704.getPort(VehicleSaleContract.class);
		BindingProvider bp = (BindingProvider) port;
		bp.getRequestContext()
				.put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY,
						URI.create(String.join("/", String.valueOf(baseURI), ESB_SERVICE_ENDPOINT)));

		return port;
	}
}
