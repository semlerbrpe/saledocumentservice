package dk.semlerit.autocore.service.salecontractservice;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import dk.semlerit.autocore.web.annotation.CacheQualifier;
import dk.semlerit.autocore.web.annotation.CacheScope;
import dk.semlerit.autocore.web.cache.Cache;
import dk.semlerit.autocore.ws.annotation.ConsumerId;
import dk.semlerit.autocore.ws.converter.wstype.DocumentWsTypeConverter;
import dk.semlerit.autocore.ws.converter.wstype.VehiclePurchasedByOwnerWsTypeConverter;
import dk.semlerit.autocore.ws.saledocument.core.model.Document;
import dk.semlerit.autocore.ws.saledocument.core.model.VehiclePurchasedByOwner;
import dk.semlerit.autocore.ws.vehiclesalecontract201704.client.RetrieveSaleContractRequest;
import dk.semlerit.autocore.ws.vehiclesalecontract201704.client.RetrieveSaleContractResponse;
import dk.semlerit.autocore.ws.vehiclesalecontract201704.client.VehicleSaleContract;
import lombok.NonNull;

/**
 * 
 * 
 * @author edbbrpe
 * @version 1.0
 * @since 3.0.0
 */
@ApplicationScoped
public class DefaultSaleContractService implements SaleContractService {

	@Inject
	@ConsumerId
	private String consumerId;
	@Inject
	@CacheQualifier(CacheScope.SERVER)
	private Cache serverCache;

	@Inject
	private VehicleSaleContract client;

	private DocumentWsTypeConverter documentWsTypeConverter = new DocumentWsTypeConverter();
	private VehiclePurchasedByOwnerWsTypeConverter vehiclePurchasedByOwnerWsTypeConverter = new VehiclePurchasedByOwnerWsTypeConverter();

	/**
	 * @throws NullPointerException
	 *             if saleContractDocumentId or dealerNumber is {@literal null}.
	 */
	@Override
	public Document retrieveDocument(@NonNull String saleContractDocumentId, @NonNull String dealerNumber) {

		RetrieveSaleContractRequest retrieveSaleContractRequest = new RetrieveSaleContractRequest();
		retrieveSaleContractRequest.setSaleContractDocumentId(saleContractDocumentId);
		retrieveSaleContractRequest.setDealerNumber(dealerNumber);
		retrieveSaleContractRequest.setConsumerId(consumerId);
		retrieveSaleContractRequest.setTransactionId("1234567890123");

		final RetrieveSaleContractResponse retrieveSaleContractResponse = client
				.retrieveSaleContract(retrieveSaleContractRequest);

		Document document = documentWsTypeConverter.convert(retrieveSaleContractResponse);
		VehiclePurchasedByOwner vehiclePurchasedByOwner = vehiclePurchasedByOwnerWsTypeConverter
				.convert(retrieveSaleContractResponse.getVehiclePurchasedByOwner());

		return null;
	}
}
