package dk.semlerit.autocore.service.salecontractservice;

import dk.semlerit.autocore.exception.ServiceClientException;
import dk.semlerit.autocore.exception.ServiceException;
import dk.semlerit.autocore.ws.saledocument.core.model.Document;

/**
 * 
 * 
 * @author edbbrpe
 * @version 1.0
 * @since 3.0.0
 */
public interface SaleContractService {

	/**
	 * Retrieve a Sale Contract by its unique identifier.
	 * 
	 * @param saleContractId
	 * @param dealerNumber
	 * @return a salecontract or {@literal null} if no SaleContract exists
	 * @throws ServiceException
	 * @throws ServiceClientException
	 * 
	 */
	Document retrieveDocument(String saleContractId, String dealerNumber);

}
