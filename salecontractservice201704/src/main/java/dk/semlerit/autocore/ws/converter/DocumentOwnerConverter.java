package dk.semlerit.autocore.ws.converter;

/**
 * 
 * 
 * @author edbbrpe
 * @version 1.0
 * @since 3.0.0
 */
public abstract class DocumentOwnerConverter {
	protected static enum SERVICE_MAKE {
		VW, VWVAN, AUDI, SKODA, SEAT, PORSCHE;
	};

	protected static final String MAKE_VW = "SP.BRAND.VW";
	protected static final String MAKE_VWVAN = "SP.BRAND.VWVAN";
	protected static final String MAKE_AUDI = "SP.BRAND.AUDI";
	protected static final String MAKE_SKODA = "SP.BRAND.SKODA";
	protected static final String MAKE_SEAT = "SP.BRAND.SEAT";
	protected static final String MAKE_PORSCHE = "SP.BRAND.PORSCHE";

}
