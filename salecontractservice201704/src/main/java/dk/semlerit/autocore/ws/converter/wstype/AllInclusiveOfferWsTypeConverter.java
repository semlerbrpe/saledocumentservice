package dk.semlerit.autocore.ws.converter.wstype;

import dk.semlerit.autocore.ws.converter.GenericConverter;
import dk.semlerit.autocore.ws.saledocument.core.model.AllInclusive;
import dk.semlerit.autocore.ws.saledocument.core.model.AllInclusiveValue;
import dk.semlerit.autocore.ws.vehiclesalecontract201704.client.AllInclusiveType;
import dk.semlerit.autocore.ws.vehiclesalecontract201704.client.AllInclusiveValueType;

/**
 * 
 * 
 * @author edbbrpe
 * @version 1.0
 * @since 3.0.0
 */
public class AllInclusiveOfferWsTypeConverter implements GenericConverter<AllInclusiveType, AllInclusive> {

	@Override
	public AllInclusive apply(AllInclusiveType t) {
		return AllInclusive.builder()
				.id(t.getId())
				.providerId(t.getProviderId())
				.allInclusiveValues(new AllInclusiveValueTypeWsTypeConverter().convert(t.getAllInclusiveValues()))
				.pdf(t.getPdf())
				.build();
	}

	static class AllInclusiveValueTypeWsTypeConverter
			implements GenericConverter<AllInclusiveValueType, AllInclusiveValue> {

		@Override
		public AllInclusiveValue apply(AllInclusiveValueType t) {
			return AllInclusiveValue.builder()
					.key(t.getKey())
					.label(t.getLabel())
					.value(t.getValue())
					.build();
		}
	}
}
