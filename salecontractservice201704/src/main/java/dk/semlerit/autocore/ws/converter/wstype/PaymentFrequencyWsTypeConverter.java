package dk.semlerit.autocore.ws.converter.wstype;

import dk.semlerit.autocore.ws.converter.GenericConverter;
import dk.semlerit.autocore.ws.vehiclesalecontract201704.client.ServiceContractType;

/**
 * 
 * 
 * @author edbbrpe
 * @version 1.0
 * @since 3.0.0
 */
public class PaymentFrequencyWsTypeConverter implements
		GenericConverter<ServiceContractType.PaymentFrequency, dk.semlerit.autocore.ws.saledocument.core.model.PaymentFrequency> {

	@Override
	public dk.semlerit.autocore.ws.saledocument.core.model.PaymentFrequency apply(
			ServiceContractType.PaymentFrequency t) {
		return dk.semlerit.autocore.ws.saledocument.core.model.PaymentFrequency.builder()
				.id(t.getId())
				.frequency(t.getFrequency())
				.frequencyDescription(t.getFrequencyDescription())
				.build();
	}
}
