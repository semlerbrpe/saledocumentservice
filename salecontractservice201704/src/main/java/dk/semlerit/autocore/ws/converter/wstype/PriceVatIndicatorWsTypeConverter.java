package dk.semlerit.autocore.ws.converter.wstype;

import dk.semlerit.autocore.ws.converter.GenericConverter;

/**
 * 
 * @author edbbrpe
 * @version 1.0
 * @Since 3.0.0
 */
public class PriceVatIndicatorWsTypeConverter implements GenericConverter<String, Integer> {

	public static final String VAT_INDICATOR_INCL_VAT_STRING = "incl.";
	public static final String VAT_INDICATOR_EXCL_VAT_STRING = "excl.";

	private static final Integer VAT_INDICATOR_INCL_VAT = 0;
	private static final Integer VAT_INDICATOR_EXCL_VAT = 1;

	@Override
	public Integer apply(String t) {
		if (VAT_INDICATOR_INCL_VAT_STRING.equalsIgnoreCase(t)) {
			return VAT_INDICATOR_INCL_VAT;
		} else if (VAT_INDICATOR_EXCL_VAT_STRING.equalsIgnoreCase(t)) {
			return VAT_INDICATOR_EXCL_VAT;
		} else {
			throw new IllegalArgumentException(String.format("Could not convert WS type value: %s to bean value.", t));
		}
	}
}
