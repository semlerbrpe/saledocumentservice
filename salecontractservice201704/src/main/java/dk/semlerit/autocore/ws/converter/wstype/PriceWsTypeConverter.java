package dk.semlerit.autocore.ws.converter.wstype;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;

import dk.semlerit.autocore.ws.converter.GenericConverter;

/**
 * 
 * 
 * @author edbbrpe
 * @version 1.0
 * @since 3.0.0
 */
public class PriceWsTypeConverter implements GenericConverter<String, BigDecimal> {

	@Override
	public BigDecimal apply(String t) {
		DecimalFormat df = (DecimalFormat) NumberFormat.getInstance(Locale.US);
		df.setParseBigDecimal(true);

		try {
			return (BigDecimal) df.parseObject(t);
		} catch (ParseException e) {
			throw new IllegalArgumentException(String.format("Could not convert %s to BigDecimal", t));
		}
	}
}
