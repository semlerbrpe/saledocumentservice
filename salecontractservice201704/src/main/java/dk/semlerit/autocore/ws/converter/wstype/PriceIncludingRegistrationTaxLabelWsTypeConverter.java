package dk.semlerit.autocore.ws.converter.wstype;

import dk.semlerit.autocore.ws.converter.GenericConverter;

/**
 * 
 * 
 * @author edbbrpe
 * @version 1.0
 * @since 3.0.0
 */
public class PriceIncludingRegistrationTaxLabelWsTypeConverter implements GenericConverter<String, Integer> {

	public static final String TAX_TYPE_EXCL_TAX_STRING = "ExclTax";
	public static final String TAX_TYPE_INCL_TAX_STRING = "InclTax";

	private static final Integer TAX_TYPE_INCL_TAX = 0;
	private static final Integer TAX_TYPE_EXCL_TAX = 1;

	@Override
	public Integer apply(String t) {
		if (TAX_TYPE_INCL_TAX_STRING.equalsIgnoreCase(t)) {
			return TAX_TYPE_INCL_TAX;
		} else if (TAX_TYPE_EXCL_TAX_STRING.equalsIgnoreCase(t)) {
			return TAX_TYPE_EXCL_TAX;
		} else {
			throw new IllegalArgumentException(String.format("Could not convert WS type value: %s to bean value.", t));
		}
	}
}
