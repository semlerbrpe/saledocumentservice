package dk.semlerit.autocore.ws.converter.bean;

import dk.semlerit.autocore.ws.converter.DocumentOwnerConverter;
import dk.semlerit.autocore.ws.converter.GenericConverter;

/**
 * 
 * 
 * @author edbbrpe
 * @version 1.0
 * @since 3.0.0
 */
public class DocumentOwnerBeanConverter extends DocumentOwnerConverter implements GenericConverter<String, String> {

	@Override
	public String apply(String b) {
		switch (b.toUpperCase()) {
		case MAKE_VW:
			return SERVICE_MAKE.VW.name();
		case MAKE_VWVAN:
			return SERVICE_MAKE.VWVAN.name();
		case MAKE_AUDI:
			return SERVICE_MAKE.AUDI.name();
		case MAKE_SKODA:
			return SERVICE_MAKE.SKODA.name();
		case MAKE_SEAT:
			return SERVICE_MAKE.SEAT.name();
		case MAKE_PORSCHE:
			return SERVICE_MAKE.PORSCHE.name();
		default:
			throw new IllegalArgumentException(
					String.format("Bean with value: %s could not be converted to documentOwner bean value", b));
		}
	}
}
