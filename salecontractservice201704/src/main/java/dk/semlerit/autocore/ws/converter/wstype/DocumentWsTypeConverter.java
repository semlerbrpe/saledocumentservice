package dk.semlerit.autocore.ws.converter.wstype;

import dk.semlerit.autocore.ws.converter.GenericConverter;
import dk.semlerit.autocore.ws.saledocument.core.model.Document;
import dk.semlerit.autocore.ws.saledocument.core.model.DocumentStatus;
import dk.semlerit.autocore.ws.saledocument.core.model.DocumentType;
import dk.semlerit.autocore.ws.vehiclesalecontract201704.client.RetrieveSaleContractResponse;

/**
 * @author edbbrpe
 * @version 1.0
 * @since 3.0.0
 *
 */
public class DocumentWsTypeConverter implements GenericConverter<RetrieveSaleContractResponse, Document> {

	@Override
	public Document apply(RetrieveSaleContractResponse t) {
		return Document.builder().documentType(DocumentType.SALECONTRACT)
				.status(DocumentStatus.getByValue(t.getStatus()))
				.originalStatus(DocumentStatus.getByValue(t.getStatus())).createdByService(t.isCreatedByService())
				.sellerName(t.getSellerName()).sellerEmail(t.getSellerEmail()).buyerName(t.getBuyerName())
				.fullNotesDocumentURL(t.getFullNotesDocumentURL()).statusCaseNumber(t.getStatusCaseNumber())
				.additionalAgreementComments(t.getAdditionalAgreementComments())
				.warrantyConditions(t.getWarrantyConditions()).date(t.getDate())
				.acceptedMarketing(t.isAcceptedMarketing()).vehicleOwnershipPOA(t.isVehicleOwnershipPOA())
				.saleOrderId(t.getSaleOrderId()).vehicleOrderId(t.getVehicleOrderId()).archived(t.isIsArchived())
				.purchasedByOwnerEconomyDraftPresent(t.isHasVehiclePurchasedByOwnerEconomyDraft())
				.soldToDealerEconomyDraftPresent(t.isHasVehicleSoldToDealerEconomyDraft())
				.lastModifiedDate(t.getLastModifiedDate()).version(Integer.toString(t.getVersion())).build();
	}
}
