package dk.semlerit.autocore.ws.converter.wstype;

import dk.semlerit.autocore.ws.converter.DocumentOwnerConverter;
import dk.semlerit.autocore.ws.converter.GenericConverter;

/**
 * 
 * 
 * @author edbbrpe
 * @version 1.0
 * @since 3.0.0
 */
public class DocumentOwnerWsTypeConverter extends DocumentOwnerConverter implements GenericConverter<String, String> {

	@Override
	public String apply(String t) {
		switch (SERVICE_MAKE.valueOf(t.toUpperCase())) {
		case AUDI:
			return MAKE_AUDI;
		case PORSCHE:
			return MAKE_PORSCHE;
		case SEAT:
			return MAKE_SEAT;
		case SKODA:
			return MAKE_SKODA;
		case VW:
			return MAKE_VW;
		case VWVAN:
			return MAKE_VWVAN;
		default:
			throw new IllegalArgumentException(
					String.format("WS Type with value: %s could not be converted to documentOwner bean value", t));
		}
	}
}
