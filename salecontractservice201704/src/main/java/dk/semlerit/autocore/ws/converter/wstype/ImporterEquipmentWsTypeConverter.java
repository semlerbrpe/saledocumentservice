package dk.semlerit.autocore.ws.converter.wstype;

import dk.semlerit.autocore.ws.converter.GenericConverter;
import dk.semlerit.autocore.ws.saledocument.core.model.ImporterEquipment;
import dk.semlerit.autocore.ws.vehiclesalecontract201704.client.ImporterEquipmentType;

/**
 * 
 * @author edbbrpe
 * @version 1.0
 * @since 3.0.0
 */
public class ImporterEquipmentWsTypeConverter implements GenericConverter<ImporterEquipmentType, ImporterEquipment> {

	@Override
	public ImporterEquipment apply(ImporterEquipmentType t) {
		return ImporterEquipment.builder()
				.code(t.getCode())
				.description(t.getDescription())
				.priceInclVAT(t.getPriceIncludingVAT())
				.priceExclVAT(t.getPriceExcludingVAT())
				.listPriceExclVAT(t.getListPriceExcludingVAT())
				.build();
	}
}
