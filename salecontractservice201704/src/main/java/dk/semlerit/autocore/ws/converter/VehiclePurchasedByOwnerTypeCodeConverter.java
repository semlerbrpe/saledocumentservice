package dk.semlerit.autocore.ws.converter;

import java.util.HashMap;
import java.util.Map;

/**
 * 
 * 
 * @author edbbrpe
 * @version 1.0
 * @since 3.0.0
 */
public abstract class VehiclePurchasedByOwnerTypeCodeConverter {
	private static final String CAR_TYPE_COACH_STRING = "Personvogn";
	private static final String CAR_TYPE_VAN_INCL_VAT_30_STRING = "Varevogn incl. moms 30%";
	private static final String CAR_TYPE_VAN_EXCL_VAT_30_STRING = "Varevogn excl. moms 30%";
	private static final String CAR_TYPE_VAN_INCL_VAT_50_STRING = "Varevogn incl. moms 50%";
	private static final String CAR_TYPE_VAN_EXCL_VAT_50_STRING = "Varevogn excl. moms 50%";
	private static final String CAR_TYPE_TAXI_STRING = "Hyrevogn";
	private static final String CAR_TYPE_EXPORT_CAR_STRING = "Export bil";
	private static final String CAR_TYPE_MOTORHOME_STRING = "Autocamper";
	private static final String CAR_TYPE_VAN_INCL_VAT_STRING = "Varevogn incl. moms";
	private static final String CAR_TYPE_VAN_EXCL_VAT_STRING = "Varevogn excl. moms";
	private static final String CAR_TYPE_COACH_INCL_VAT_STRING = "Personvogn incl. moms";

	private static final Integer CAR_TYPE_COACH = 0;
	private static final Integer CAR_TYPE_VAN_INCL_VAT_30 = 1;
	private static final Integer CAR_TYPE_VAN_EXCL_VAT_30 = 2;
	private static final Integer CAR_TYPE_MOTORHOME = 3;
	private static final Integer CAR_TYPE_TAXI = 4;
	private static final Integer CAR_TYPE_EXPORT_CAR = 5;
	private static final Integer CAR_TYPE_VAN_INCL_VAT_50 = 6;
	private static final Integer CAR_TYPE_VAN_EXCL_VAT_50 = 7;
	private static final Integer CAR_TYPE_VAN_EXCL_VAT = 8;
	private static final Integer CAR_TYPE_VAN_INCL_VAT = 9;
	private static final Integer CAR_TYPE_COACH_INCL_VAT = 10;

	protected static final Map<Integer, String> BEAN_TO_TYPE_MAP = new HashMap<Integer, String>() {
		private static final long serialVersionUID = -5566408556778361077L;
		{
			put(CAR_TYPE_COACH, CAR_TYPE_COACH_STRING);
			put(CAR_TYPE_VAN_INCL_VAT_30, CAR_TYPE_VAN_INCL_VAT_30_STRING);
			put(CAR_TYPE_VAN_EXCL_VAT_30, CAR_TYPE_VAN_EXCL_VAT_30_STRING);
			put(CAR_TYPE_VAN_INCL_VAT_50, CAR_TYPE_VAN_INCL_VAT_50_STRING);
			put(CAR_TYPE_VAN_EXCL_VAT_50, CAR_TYPE_VAN_EXCL_VAT_50_STRING);
			put(CAR_TYPE_TAXI, CAR_TYPE_TAXI_STRING);
			put(CAR_TYPE_EXPORT_CAR, CAR_TYPE_EXPORT_CAR_STRING);
			put(CAR_TYPE_MOTORHOME, CAR_TYPE_MOTORHOME_STRING);
			put(CAR_TYPE_VAN_INCL_VAT, CAR_TYPE_VAN_INCL_VAT_STRING);
			put(CAR_TYPE_VAN_EXCL_VAT, CAR_TYPE_VAN_EXCL_VAT_STRING);
			put(CAR_TYPE_COACH_INCL_VAT, CAR_TYPE_COACH_INCL_VAT_STRING);
		}
	};

	protected static final Map<String, Integer> TYPE_TO_BEAN_MAP = new HashMap<String, Integer>() {
		private static final long serialVersionUID = 4078388037571504731L;
		{
			put(CAR_TYPE_COACH_STRING, CAR_TYPE_COACH);
			put(CAR_TYPE_VAN_INCL_VAT_30_STRING, CAR_TYPE_VAN_INCL_VAT_30);
			put(CAR_TYPE_VAN_EXCL_VAT_30_STRING, CAR_TYPE_VAN_EXCL_VAT_30);
			put(CAR_TYPE_VAN_INCL_VAT_50_STRING, CAR_TYPE_VAN_INCL_VAT_50);
			put(CAR_TYPE_VAN_EXCL_VAT_50_STRING, CAR_TYPE_VAN_EXCL_VAT_50);
			put(CAR_TYPE_TAXI_STRING, CAR_TYPE_TAXI);
			put(CAR_TYPE_EXPORT_CAR_STRING, CAR_TYPE_EXPORT_CAR);
			put(CAR_TYPE_MOTORHOME_STRING, CAR_TYPE_MOTORHOME);
			put(CAR_TYPE_VAN_INCL_VAT_STRING, CAR_TYPE_VAN_INCL_VAT);
			put(CAR_TYPE_VAN_EXCL_VAT_STRING, CAR_TYPE_VAN_EXCL_VAT);
			put(CAR_TYPE_COACH_INCL_VAT_STRING, CAR_TYPE_COACH_INCL_VAT);
		}
	};
}
