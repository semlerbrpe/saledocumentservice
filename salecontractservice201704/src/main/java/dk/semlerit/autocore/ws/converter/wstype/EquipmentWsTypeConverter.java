package dk.semlerit.autocore.ws.converter.wstype;

import dk.semlerit.autocore.ws.converter.GenericConverter;
import dk.semlerit.autocore.ws.saledocument.core.model.Equipment;
import dk.semlerit.autocore.ws.vehiclesalecontract201704.client.EquipmentType;

/**
 * 
 * @author edbbrpe
 * @version 1.0
 * @since 3.0.0
 */
public class EquipmentWsTypeConverter implements GenericConverter<EquipmentType, Equipment> {

	@Override
	public Equipment apply(EquipmentType t) {
		return Equipment.builder().code(t.getCode()).description(t.getDescription())
				.priceIncludingVAT(t.getPriceIncludingVAT()).priceExcludingVAT(t.getPriceExcludingVAT()).build();
	}
}
