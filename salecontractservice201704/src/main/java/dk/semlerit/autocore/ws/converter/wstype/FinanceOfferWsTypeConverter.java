package dk.semlerit.autocore.ws.converter.wstype;

import dk.semlerit.autocore.ws.converter.GenericConverter;
import dk.semlerit.autocore.ws.saledocument.core.model.FinanceOffer;
import dk.semlerit.autocore.ws.saledocument.core.model.FinanceValue;
import dk.semlerit.autocore.ws.vehiclesalecontract201704.client.FinanceOfferType;
import dk.semlerit.autocore.ws.vehiclesalecontract201704.client.FinanceValueType;

/**
 * 
 * 
 * @author edbbrpe
 * @version 1.0
 * @since 3.0.0
 */
public class FinanceOfferWsTypeConverter implements GenericConverter<FinanceOfferType, FinanceOffer> {

	@Override
	public FinanceOffer apply(FinanceOfferType t) {
		FinanceOffer financeOffer = FinanceOffer.builder()
				.id(t.getId())
				.providerId(t.getProviderId())
				.pdfPDF(t.getPdf())
				.build();

		financeOffer.setFinanceValues(new FinanceValueWsTypeConverter().convert(t.getFinanceValues()));

		return financeOffer;
	}

	static class FinanceValueWsTypeConverter implements GenericConverter<FinanceValueType, FinanceValue> {

		@Override
		public FinanceValue apply(FinanceValueType t) {
			return FinanceValue.builder()
					.key(t.getKey())
					.label(t.getLabel())
					.value(t.getValue())
					.build();
		}
	}
}
