package dk.semlerit.autocore.ws.converter.wstype;

import dk.semlerit.autocore.ws.converter.GenericConverter;
import dk.semlerit.autocore.ws.saledocument.core.model.InsuranceOffer;
import dk.semlerit.autocore.ws.saledocument.core.model.InsuranceValue;
import dk.semlerit.autocore.ws.vehiclesalecontract201704.client.InsuranceOfferType;
import dk.semlerit.autocore.ws.vehiclesalecontract201704.client.InsuranceValueType;

/**
 * 
 * 
 * @author edbbrpe
 * @version 1.0
 * @since 3.0.0
 */
public class InsuranceOfferWsTypeConverter implements GenericConverter<InsuranceOfferType, InsuranceOffer> {

	@Override
	public InsuranceOffer apply(InsuranceOfferType t) {
		return InsuranceOffer.builder()
				.id(t.getId())
				.providerId(t.getProviderId())
				.pdf(t.getPdf())
				.insuranceValues(new InsuranceValueWsTypeConverter().convert(t.getInsuranceValues()))
				.build();
	}

	static class InsuranceValueWsTypeConverter implements GenericConverter<InsuranceValueType, InsuranceValue> {

		@Override
		public InsuranceValue apply(InsuranceValueType t) {
			return InsuranceValue.builder()
					.key(t.getKey())
					.label(t.getLabel())
					.value(t.getValue())
					.build();
		}
	}
}
