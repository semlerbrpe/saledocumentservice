package dk.semlerit.autocore.ws.converter.wstype;

import dk.semlerit.autocore.ws.converter.GenericConverter;
import dk.semlerit.autocore.ws.saledocument.core.model.ContactInformation;
import dk.semlerit.autocore.ws.saledocument.core.model.UserType;
import dk.semlerit.autocore.ws.vehiclesalecontract201704.client.ContactInformationType;

/**
 * 
 * @author edbbrpe
 * @version 1.0
 * @since 3.0.0
 */
public class ContactInformationWsTypeConverter implements GenericConverter<ContactInformationType, ContactInformation> {

	@Override
	public ContactInformation apply(ContactInformationType t) {
		return ContactInformation.builder()
				.customerName(t.getCustomerName())
				.doNotUseMail(t.isDoNotUseMail())
				.doNotUsePhone(t.isDoNotUsePhone())
				.email(t.getEmail())
				.phone(t.getPhone())
				.userTypeInformationSelected(UserType.getByField(t.getUseCustomerInformationSelected()))
				.build();
	}
}
