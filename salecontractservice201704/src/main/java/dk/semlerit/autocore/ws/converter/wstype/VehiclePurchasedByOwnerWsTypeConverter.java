package dk.semlerit.autocore.ws.converter.wstype;

import java.util.Collections;
import java.util.Optional;

import dk.semlerit.autocore.ws.converter.GenericConverter;
import dk.semlerit.autocore.ws.converter.VehiclePurchasedByOwnerEconomyWsTypeConverter;
import dk.semlerit.autocore.ws.saledocument.core.model.VehicleCondition;
import dk.semlerit.autocore.ws.saledocument.core.model.VehicleMake;
import dk.semlerit.autocore.ws.saledocument.core.model.VehiclePurchasedByOwner;
import dk.semlerit.autocore.ws.vehiclesalecontract201704.client.VehiclePurchasedByOwnerType;

/**
 * 
 * @author edbbrpe
 * @version 1.0
 * @since 3.0.0
 */
public class VehiclePurchasedByOwnerWsTypeConverter
		implements GenericConverter<VehiclePurchasedByOwnerType, VehiclePurchasedByOwner> {

	@Override
	public VehiclePurchasedByOwner apply(VehiclePurchasedByOwnerType t) {
		VehiclePurchasedByOwner vehiclePurchasedByOwner = VehiclePurchasedByOwner.builder()
				.orderType(t.getOrderType())
				.make(VehicleMake.getByValue(t.getMake()))
				.variantDocumentId(t.getVariantDocumentId())
				.licensePlate(t.getVpbcLicensePlate())
				.vin(t.getIdentificationNumber())
				.scheduleCode(t.getCardNumber())
				.insuranceCompany(t.getInsuranceCompany())
				.insurancePolicyNumber(t.getInsurancePolicyNumber())
				.deliveryDate(t.getDeliveryDate())
				.deliveryTime(t.getDeliveryTime())
				.deliveryWeek(t.getDeliveryWeek())
				.deliveryYear(t.getDeliveryYear())
				.deliveryArranged(t.isDeliveryIsArranged())
				.vehicleCondition(VehicleCondition.getByValue(t.getCondition()))
				.modelText(t.getVpbcModel())
				.vehicleType(t.getVehicleType())
				.color(t.getVpbcColor())
				.interior(t.getInterior())
				.firstRegistrationDate(t.getVpbcFirstRegistrationDate())
				.productionYear(t.getProductionYear())
				.vehicleCondition(VehicleCondition.getByValue(t.getCondition()))
				.kilometers(t.getVpbcKilometers())
				.factoryEquipment(t.getVpbcFactoryEquipment())
				.vehicleCondition(VehicleCondition.getByValue(t.getCondition()))
				.build();

		vehiclePurchasedByOwner.setEuNumberPlate(new EUNumberPlateWSTypeConverter().convert(t.getEuNumberPlate()));
		vehiclePurchasedByOwner.setType(new VehiclePurchasedByOwnerTypeCodeWsTypeConverter().convert(t.getVpbcType()));

		vehiclePurchasedByOwner.setStandardEquipments(
				new EquipmentWsTypeConverter().convert(Optional.ofNullable(t.getStandardEquipments())
						.orElseGet(() -> Collections.emptyList())));
		vehiclePurchasedByOwner.setStandardEquipmentDeselecteds(
				new EquipmentWsTypeConverter().convert(Optional.ofNullable(t.getStandardEquipmentDeselecteds())
						.orElseGet(() -> Collections.emptyList())));
		vehiclePurchasedByOwner.setAdditionalEquipments(
				new EquipmentWsTypeConverter().convert(Optional.ofNullable(t.getAdditionalEquipments())
						.orElseGet(() -> Collections.emptyList())));
		vehiclePurchasedByOwner.setDealerEquipments(
				new EquipmentWsTypeConverter().convert(Optional.ofNullable(t.getAdditionalEquipments())
						.orElseGet(() -> Collections.emptyList())));
		vehiclePurchasedByOwner.setImporterEquipments(
				new ImporterEquipmentWsTypeConverter().convert(Optional.ofNullable(t.getImporterEquipments())
						.orElseGet(() -> Collections.emptyList())));
		vehiclePurchasedByOwner.setVehiclePurchasedByOwnerEconomy(
				new VehiclePurchasedByOwnerEconomyWsTypeConverter().convert(t.getEconomy()));

		vehiclePurchasedByOwner
				.setContactInformation(new ContactInformationWsTypeConverter().convert(t.getContactInformation()));

		vehiclePurchasedByOwner
				.setServiceContract(new ServiceContractWsTypeConverter().convert(t.getServiceContract()));

		vehiclePurchasedByOwner
				.setExtendedWarranty(new ExtendedWarrantyWsTypeConverter().convert(t.getExtendedWarranty()));

		vehiclePurchasedByOwner.setSalesChannel(new SalesChannelWsTypeConverter().convert(t.getSalesChannel()));

		vehiclePurchasedByOwner.setFinanceOffer(new FinanceOfferWsTypeConverter().convert(t.getFinanceOffer()));
		vehiclePurchasedByOwner.setInsuranceOffer(new InsuranceOfferWsTypeConverter().convert(t.getInsuranceOffer()));
		vehiclePurchasedByOwner.setLeaseOffer(new LeaseOfferTypeWsTypeConverter().convert(t.getLeaseOfferType()));
		vehiclePurchasedByOwner
				.setAllInclusive(new AllInclusiveOfferWsTypeConverter().convert(t.getAllInclusiveOffer()));

		return vehiclePurchasedByOwner;
	}
}
