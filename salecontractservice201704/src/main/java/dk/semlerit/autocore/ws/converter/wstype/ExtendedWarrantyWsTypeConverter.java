package dk.semlerit.autocore.ws.converter.wstype;

import dk.semlerit.autocore.ws.converter.GenericConverter;
import dk.semlerit.autocore.ws.saledocument.core.model.ExtendedWarranty;
import dk.semlerit.autocore.ws.vehiclesalecontract201704.client.ExtendedWarrantyType;

/**
 * @author edbbrpe
 * @version 1.0
 * @since 3.0.0
 */
public class ExtendedWarrantyWsTypeConverter implements GenericConverter<ExtendedWarrantyType, ExtendedWarranty> {

	@Override
	public ExtendedWarranty apply(ExtendedWarrantyType t) {
		return ExtendedWarranty.builder()
				.id(t.getId())
				.priceIncludingVAT(t.getPriceIncludingVAT())
				.priceExcludingVAT(t.getPriceExcludingVAT())
				.description(t.getDescription())
				.pdf(t.getPdf())
				.VAT(t.getVAT())
				.build();
	}
}
