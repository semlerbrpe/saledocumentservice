package dk.semlerit.autocore.ws.converter.wstype;

import java.util.Collections;
import java.util.Optional;

import dk.semlerit.autocore.ws.converter.GenericConverter;
import dk.semlerit.autocore.ws.saledocument.core.model.ContractType;
import dk.semlerit.autocore.ws.saledocument.core.model.ServiceContract;
import dk.semlerit.autocore.ws.vehiclesalecontract201704.client.ServiceContractType;

/**
 * 
 * 
 * @author edbbrpe
 * @version 1.0
 * @since 3.0.0
 */
public class ServiceContractWsTypeConverter implements GenericConverter<ServiceContractType, ServiceContract> {

	OptionWsTypeConverter optionWsTypeConverter = new OptionWsTypeConverter();
	BasicCalculatedPriceWsTypeConverter basicCalculatedPriceWsTypeConverter = new BasicCalculatedPriceWsTypeConverter();
	PaymentFrequencyWsTypeConverter paymentFrequencyWsTypeConverter = new PaymentFrequencyWsTypeConverter();

	@Override
	public ServiceContract apply(ServiceContractType t) {
		ServiceContract serviceContract = ServiceContract.builder()
				.allowedKilometers(t.getAllowedKilometers())
				.contractDuration(t.getContractDuration())
				.contractType(ContractType.getByValue(t.getContractType()))
				.dealerDiscountExclVAT(t.getDealerDiscountExclVAT())
				.kilometersPerMonth(t.getKilometersPerMonth())
				.kilometersPerYear(t.getKilometersPerYear())
				.priceModelId(t.getPriceModelId())
				.options(optionWsTypeConverter.convert(Optional.ofNullable(t.getContractOptions())
						.orElseGet(() -> Collections.emptyList())))
				.basicCalculatedPrice(basicCalculatedPriceWsTypeConverter.convert(t.getBasicCalculatedPrice()))
				.paymentFrequency(paymentFrequencyWsTypeConverter.convert(t.getPaymentFrequency()))
				.build();

		return serviceContract;
	}
}
