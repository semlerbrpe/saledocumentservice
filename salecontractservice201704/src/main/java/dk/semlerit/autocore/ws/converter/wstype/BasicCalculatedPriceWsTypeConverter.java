package dk.semlerit.autocore.ws.converter.wstype;

import dk.semlerit.autocore.ws.converter.GenericConverter;
import dk.semlerit.autocore.ws.saledocument.core.model.CalculatedPrice;
import dk.semlerit.autocore.ws.vehiclesalecontract201704.client.ServiceContractType;
import dk.semlerit.autocore.ws.vehiclesalecontract201704.client.ServiceContractType.BasicCalculatedPrice;

/**
 * @author edbbrpe
 * @version 1.0
 * @since 3.0.0
 */
public class BasicCalculatedPriceWsTypeConverter
		implements GenericConverter<ServiceContractType.BasicCalculatedPrice, CalculatedPrice> {

	@Override
	public CalculatedPrice apply(BasicCalculatedPrice t) {
		return CalculatedPrice.builder()
				.pricePerPayment(t.getPricePerPayment())
				.additionalKilometersPrice(t.getAdditionalKilometersPrice())
				.contractPricePerKm(t.getContractPricePerKm())
				.totalPrice(t.getTotalPrice())
				.vatIncluded(t.isVatIncluded())
				.build();
	}
}
