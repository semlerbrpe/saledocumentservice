package dk.semlerit.autocore.ws.converter.bean;

import dk.semlerit.autocore.ws.converter.GenericConverter;
import dk.semlerit.autocore.ws.converter.VehiclePurchasedByOwnerTypeCodeConverter;

/**
 * 
 * 
 * @author edbbrpe
 * @version 1.0
 * @since 3.0.0
 */
public class VehiclePurchasedByOwnerTypeCodeBeanConverter extends VehiclePurchasedByOwnerTypeCodeConverter
		implements GenericConverter<Integer, String> {

	@Override
	public String apply(Integer b) {
		return BEAN_TO_TYPE_MAP.get(b);
	}
}
