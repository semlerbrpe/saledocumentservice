/**
 * 
 */
package dk.semlerit.autocore.ws.converter.wstype;

import dk.semlerit.autocore.ws.converter.GenericConverter;
import dk.semlerit.autocore.ws.saledocument.core.model.Option;
import dk.semlerit.autocore.ws.vehiclesalecontract201704.client.ServiceContractType;
import dk.semlerit.autocore.ws.vehiclesalecontract201704.client.ServiceContractType.ContractOptions;

/**
 * @author edbbrpe
 * @version 1.0
 * @since 3.0.0
 */
public class OptionWsTypeConverter implements GenericConverter<ServiceContractType.ContractOptions, Option> {

	@Override
	public Option apply(ContractOptions t) {
		return Option.builder()
				.optionId(t.getOptionId())
				.optionPrice(t.getOptionPrice())
				.optionDescription(t.getOptionDescription())
				.build();
	}
}
