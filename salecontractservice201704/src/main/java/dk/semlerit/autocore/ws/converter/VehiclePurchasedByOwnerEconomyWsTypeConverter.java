package dk.semlerit.autocore.ws.converter;

import dk.semlerit.autocore.ws.converter.wstype.PriceIncludingRegistrationTaxLabelWsTypeConverter;
import dk.semlerit.autocore.ws.converter.wstype.PriceVatIndicatorWsTypeConverter;
import dk.semlerit.autocore.ws.converter.wstype.PriceWsTypeConverter;
import dk.semlerit.autocore.ws.converter.wstype.PriceconfigurationWsTypeConverter;
import dk.semlerit.autocore.ws.converter.wstype.TotalPriceTypeWsTypeConverter;
import dk.semlerit.autocore.ws.saledocument.core.model.VehiclePurchasedByOwnerEconomy;
import dk.semlerit.autocore.ws.vehiclesalecontract201704.client.VehiclePurchasedByOwnerEconomyType;

/**
 * 
 * 
 * @author edbbrpe
 * @version 1.0
 * @since 3.0.0
 */
public class VehiclePurchasedByOwnerEconomyWsTypeConverter
		implements GenericConverter<VehiclePurchasedByOwnerEconomyType, VehiclePurchasedByOwnerEconomy> {

	PriceWsTypeConverter priceWsTypeConverter = new PriceWsTypeConverter();
	PriceIncludingRegistrationTaxLabelWsTypeConverter priceIncludingRegistrationTaxLabelWsTypeConverter = new PriceIncludingRegistrationTaxLabelWsTypeConverter();
	PriceVatIndicatorWsTypeConverter priceVatIndicatorWsTypeConverter = new PriceVatIndicatorWsTypeConverter();
	TotalPriceTypeWsTypeConverter totalPriceTypeWsTypeConverter = new TotalPriceTypeWsTypeConverter();
	PriceconfigurationWsTypeConverter priceconfigurationWsTypeConverter = new PriceconfigurationWsTypeConverter();

	@Override
	public VehiclePurchasedByOwnerEconomy apply(VehiclePurchasedByOwnerEconomyType t) {
		VehiclePurchasedByOwnerEconomy vehiclePurchasedByOwnerEconomy = VehiclePurchasedByOwnerEconomy.builder()
				.priceCode(t.getPriceCode())
				.price(t.getVpbcprice())
				.priceInclVAT(t.getVpbcpriceInclVat())
				.priceIncludingRegistrationTaxLabel(priceIncludingRegistrationTaxLabelWsTypeConverter
						.convert(t.getPriceIncludingRegistrationTaxLabel()))
				.priceIncludingRegistrationTax(t.getPriceIncludingRegistrationTax())
				.totalPrice(t.getVpbctotalPrice())
				.totalPriceType(totalPriceTypeWsTypeConverter.convert(t.getTotalPriceType()))
				.totalVehiclePriceIncludingVat(t.getTotalVehiclePriceIncludingVat())
				.totalVehiclePriceExcludingVat(t.getTotalVehiclePriceExcludingVat())
				.totalVehiclePriceVat(t.getTotalVehiclePriceVat())
				.totalPriceExcludingTaxIncludingVat(t.getVpbctotalPriceExcludingTaxIncludingVat())
				.totalPriceExcludingTaxExcludingVat(t.getVpbctotalPriceExcludingTaxExcludingVat())
				.vat(t.getVpbcvat())
				.priceVatIndicator(priceVatIndicatorWsTypeConverter.convert(t.getPriceVatIndicator()))
				.field1(t.getField1())
				.price1(t.getPrice1())
				.field2(t.getField2())
				.price2(t.getPrice2())
				.price3(t.getPrice3())
				.deliveryExpenses(t.getDeliveryExpenses())
				.licenseTagFee(t.getLicenseTagFee())
				.cashOnDeliveryLabel(t.getCashOnDeliveryLabel())
				.cashOnDeliveryPrice(t.getCashOnDeliveryPrice())
				.payment(t.getPayment())
				.vehicleSoldToDealerTotalPrice(t.getVehicleSoldToDealerTotalPrice())
				.remainingAmountMonths(t.getRemainingAmountMonths())
				.remainingAmountInstitute(t.getRemainingAmountInstitute())
				.remainingAmountAnnualInterest(priceWsTypeConverter.convert(t.getRemainingAmountAnnualInterest()))
				.remainingAmountMonthlyPayment(priceWsTypeConverter.convert(t.getRemainingAmountMonthlyPayment()))
				.equipmentProvidedByDealer(t.getEquipmentProvidedByDealer())
				.equipmentProvidedByFactory(t.getEquipmentProvidedByFactory())
				.calculationDate(t.getCalculationDate())
				.taxCode(t.getTaxCode())
				.build();

		vehiclePurchasedByOwnerEconomy
				.setPriceConfiguration(priceconfigurationWsTypeConverter.convert(t.getPriceConfiguration()));

		return vehiclePurchasedByOwnerEconomy;

	}
}
