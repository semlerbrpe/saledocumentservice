package dk.semlerit.autocore.ws.converter.wstype;

import dk.semlerit.autocore.ws.converter.GenericConverter;
import dk.semlerit.autocore.ws.saledocument.core.model.PriceConfiguration;
import dk.semlerit.autocore.ws.vehiclesalecontract201704.client.PriceConfigurationType;

/**
 * 
 * 
 * @author edbbrpe
 * @version 1.0
 * @since 3.0.0
 */
public class PriceconfigurationWsTypeConverter implements GenericConverter<PriceConfigurationType, PriceConfiguration> {

	@Override
	public PriceConfiguration apply(PriceConfigurationType t) {
		return PriceConfiguration.builder()
				.additionalEquipmentTotalPriceExcludingVAT(t.getAdditionalEquipmentTotalPriceExcludingVAT())
				.additionalEquipmentTotalPriceIncludingVAT(t.getAdditionalEquipmentTotalPriceIncludingVAT())
				.campaignCodes(t.getCampaignCodes())
				.carPriceExcludingVAT(t.getCarPriceExcludingVAT())
				.carPriceIncludingVAT(t.getCarPriceIncludingVAT())
				.colorCode(t.getColorCode())
				.colorCodeLabel(t.getColorCodeLabel())
				.customerDiscountAmount(t.getCustomerDiscountAmount())
				.importerEquipmentTotalPriceExcludingVAT(t.getImporterEquipmentTotalPriceExcludingVAT())
				.importerEquipmentTotalPriceIncludingVAT(t.getImporterEquipmentTotalPriceIncludingVAT())
				.dealerEquipmentTotalPriceExcludingVAT(t.getDealerEquipmentTotalPriceExcludingVAT())
				.dealerEquipmentTotalPriceIncludingVAT(t.getDealerEquipmentTotalPriceIncludingVAT())
				.dealerMargin(t.getDealerMargin())
				.discountAmount(t.getDiscountAmount())
				.interiorCode(t.getInteriorCode())
				.interiorCodeLabel(t.getInteriorCodeLabel())
				.maximumDiscount(t.getMaximumDiscount())
				.minimumStandardPrice(t.getMinimumStandardPrice())
				.standardEquipmentTotalPriceExcludingVAT(t.getStandardEquipmentTotalPriceExcludingVAT())
				.standardEquipmentTotalPriceIncludingVAT(t.getStandardEquipmentTotalPriceIncludingVAT())
				.totalPriceExcludingVAT(t.getTotalPriceExcludingVAT())
				.totalPriceIncludingVAT(t.getTotalPriceIncludingVAT())
				.taxExclRegulations(t.getTaxExclRegulations())
				.fuelRegulation(t.getFuelRegulation())
				.usedVehicleTypeIndicator(t.getUsedVehicleTypeIndicator())
				.totalTax(t.getTotalTax())
				.totalVAT(t.getTotalVAT())
				.build();
	}
}
