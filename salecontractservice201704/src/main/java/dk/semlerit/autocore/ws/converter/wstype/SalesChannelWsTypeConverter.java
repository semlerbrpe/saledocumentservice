package dk.semlerit.autocore.ws.converter.wstype;

import java.util.Collections;
import java.util.Optional;

import dk.semlerit.autocore.ws.converter.GenericConverter;
import dk.semlerit.autocore.ws.saledocument.core.model.SalesChannel;
import dk.semlerit.autocore.ws.vehiclesalecontract201704.client.SalesChannelType;

/**
 * @author edbbrpe
 * @version 1.0
 * @since 3.0.0
 */
public class SalesChannelWsTypeConverter implements GenericConverter<SalesChannelType, SalesChannel> {

	@Override
	public SalesChannel apply(SalesChannelType t) {
		return SalesChannel.builder()
				.id(t.getID())
				.name(t.getNAME())
				.activeFrom(t.getACTIVEFROM())
				.activeTo(t.getACTIVETO())
				.prNumbers(t.getPRNUMBERS())
				.paymentTypes(new PaymentTypeWsTypeConverter().convert(Optional.ofNullable(t.getPAYMENTTYPES())
						.orElseGet(() -> Collections.emptyList())))
				.build();

	}
}
