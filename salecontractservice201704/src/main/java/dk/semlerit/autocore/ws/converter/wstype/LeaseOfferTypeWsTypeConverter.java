package dk.semlerit.autocore.ws.converter.wstype;

import dk.semlerit.autocore.ws.converter.GenericConverter;
import dk.semlerit.autocore.ws.saledocument.core.model.LeaseOffer;
import dk.semlerit.autocore.ws.saledocument.core.model.LeaseValue;
import dk.semlerit.autocore.ws.vehiclesalecontract201704.client.LeaseOfferType;
import dk.semlerit.autocore.ws.vehiclesalecontract201704.client.LeaseValueType;

/**
 * 
 * 
 * @author edbbrpe
 * @version 1.0
 * @since 3.0.0
 */
public class LeaseOfferTypeWsTypeConverter implements GenericConverter<LeaseOfferType, LeaseOffer> {

	@Override
	public LeaseOffer apply(LeaseOfferType t) {
		return LeaseOffer.builder()
				.id(t.getId())
				.providerId(t.getProviderId())
				.pdf(t.getPdf())
				.leaseValues(new LeaseValueWsTypeConverter().convert(t.getLeaseValues()))
				.build();
	}

	static class LeaseValueWsTypeConverter implements GenericConverter<LeaseValueType, LeaseValue> {

		@Override
		public LeaseValue apply(LeaseValueType t) {
			return LeaseValue.builder()
					.key(t.getKey())
					.label(t.getLabel())
					.value(t.getValue())
					.build();
		}
	}
}
