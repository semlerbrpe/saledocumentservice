package dk.semlerit.autocore.ws.converter.wstype;

import dk.semlerit.autocore.ws.converter.GenericConverter;
import dk.semlerit.autocore.ws.saledocument.core.model.PaymentType;

/**
 * 
 * @author edbbrpe
 * @version 1.0
 * @since 3.0.0
 */
public class PaymentTypeWsTypeConverter implements GenericConverter<String, PaymentType> {

	@Override
	public PaymentType apply(String t) {
		return PaymentType.valueOf(t);
	}
}
