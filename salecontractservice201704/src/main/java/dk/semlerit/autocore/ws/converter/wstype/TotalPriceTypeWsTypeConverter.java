package dk.semlerit.autocore.ws.converter.wstype;

import dk.semlerit.autocore.ws.converter.GenericConverter;

/**
 * 
 * 
 * @author edbbrpe
 * @version 1.0
 * @Since 3.0.0
 */
public class TotalPriceTypeWsTypeConverter implements GenericConverter<String, Integer> {

	public static final String TOTAL_PRICE_TYPE_OUTSTANDING_STRING = "Restbeløb";
	public static final String TOTAL_PRICE_TYPE_EXCHANGE_STRING = "Byttepris";
	public static final String TOTAL_PRICE_TYPE_CASH_STRING = "Kontantpris";

	private static final int TOTAL_PRICE_TYPE_NOT_SPECIFIED = -1;
	private static final int TOTAL_PRICE_TYPE_OUTSTANDING = 0;
	private static final int TOTAL_PRICE_TYPE_EXCHANGE = 1;
	private static final int TOTAL_PRICE_TYPE_CASH = 2;

	@Override
	public Integer apply(String t) {
		if (TOTAL_PRICE_TYPE_OUTSTANDING_STRING.equalsIgnoreCase(t)) {
			return TOTAL_PRICE_TYPE_OUTSTANDING;
		} else if (TOTAL_PRICE_TYPE_EXCHANGE_STRING.equalsIgnoreCase(t)) {
			return TOTAL_PRICE_TYPE_EXCHANGE;
		} else if (TOTAL_PRICE_TYPE_CASH_STRING.equalsIgnoreCase(t)) {
			return TOTAL_PRICE_TYPE_CASH;
		} else {
			return TOTAL_PRICE_TYPE_NOT_SPECIFIED;
		}
	}
}
