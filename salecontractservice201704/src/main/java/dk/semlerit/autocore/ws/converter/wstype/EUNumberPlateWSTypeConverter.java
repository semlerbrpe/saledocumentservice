package dk.semlerit.autocore.ws.converter.wstype;

import dk.semlerit.autocore.ws.converter.GenericConverter;

/**
 * 
 * @author edbbrpe
 * @version 1.0
 * @since 3.0.0
 */
public class EUNumberPlateWSTypeConverter implements GenericConverter<String, Boolean> {

	private static final String TRUE_VALUE = "Ja";

	@Override
	public Boolean apply(String t) {
		return Boolean.valueOf(TRUE_VALUE.equalsIgnoreCase(t));
	}
}
