package dk.semlerit.autocore.ws.converter.wstype;

import dk.semlerit.autocore.ws.converter.GenericConverter;
import dk.semlerit.autocore.ws.converter.VehiclePurchasedByOwnerTypeCodeConverter;

/**
 * 
 * 
 * @author edbbrpe
 * @version 1.0
 * @since 3.0.0
 */
public class VehiclePurchasedByOwnerTypeCodeWsTypeConverter extends VehiclePurchasedByOwnerTypeCodeConverter
		implements GenericConverter<String, Integer> {

	@Override
	public Integer apply(String t) {
		return TYPE_TO_BEAN_MAP.get(t);
	}
}
