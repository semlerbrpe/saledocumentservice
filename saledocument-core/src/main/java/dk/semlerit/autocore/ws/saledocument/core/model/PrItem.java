package dk.semlerit.autocore.ws.saledocument.core.model;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @author edbbrpe
 *
 */
@Getter
@Setter
@ToString
public class PrItem implements Serializable {
	private static final long serialVersionUID = 6086198114226510196L;

	private String prNo;
	private String prTxt;
	private String prStd;
	private boolean prConf;

}
