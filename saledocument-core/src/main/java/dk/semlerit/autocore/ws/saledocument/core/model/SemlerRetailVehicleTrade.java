package dk.semlerit.autocore.ws.saledocument.core.model;

import java.io.Serializable;
import java.math.BigDecimal;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @author extbda
 * @author mi
 * @author lafl
 * @author extthtb
 * 
 * @version 3.0
 * @since 2.6.0
 */
@Getter
@Setter
@ToString
public class SemlerRetailVehicleTrade implements Serializable {
	private static final long serialVersionUID = -8969218611063006424L;

	private SRTradeType tradeType;
	private SRDepartmentNumber departmentNumber;
	private String employeeNumber;
	private BigDecimal sellingPrice;
	private BigDecimal fee;

}