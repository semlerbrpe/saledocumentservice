package dk.semlerit.autocore.ws.saledocument.core.model;

import java.io.Serializable;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Portlet TO to transfer Customer contact information to the service layer
 * 
 * @author extbda
 * @author mi
 * @author lafl
 * @author extthtb
 * 
 * @version 3.0
 * @since 2.6.0
 */
@Getter
@Setter
@Builder
@ToString
public class ContactInformation implements Serializable {
	private static final long serialVersionUID = 2902901940058672347L;
	private String customerName;
	private String phone;
	private String email;
	private UserType userTypeInformationSelected;
	private Boolean doNotUsePhone;
	private Boolean doNotUseMail;

}
