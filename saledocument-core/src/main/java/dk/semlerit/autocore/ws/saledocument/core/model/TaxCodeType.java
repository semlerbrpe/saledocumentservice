package dk.semlerit.autocore.ws.saledocument.core.model;

import lombok.Getter;

/**
 * Describes the different tax code types. New tax code types should ideally not
 * be added to this list.
 * 
 * @author extbda
 * @author mi
 * @author lafl
 * @author extthtb
 * 
 * @version 3.0
 * @since 2.6.0
 */
@Deprecated
@Getter
public enum TaxCodeType {
	NOT_SPECIFIED("  ", "Choose default tax code specified by importer"), INDIVIDUAL("I ",
			"Individuel afgiftsberegning"), COACH("PE", "Personvogn 150% afgift"), COACH_P3("P3",
					"PERSONVOGN 150 % AFGIFT (NY KN�KSATS 185.000)"), COACHELECTRIC("EL",
							"EL biler, Personvogn 150% afgift"), COACHHYBRID("HB",
									"Hybrid benzin, Personvogn 150% afgift"), COACH2016("PX",
											"Personvogn 150% afgift pr.1.1.2016"), COACH180("PB",
													"Personvogn 150% afgift"), TAXI("HY",
															"Hyrevogn 0% afgift"), AMBULANCE_SERVICE("SY",
																	"Sygetransport 0% afgift"), VAN30V("V3",
																			"Varevogn 30% afgift, uden max afgift"), VAN30V2016(
																					"X3",
																					"Varevogn 30% afgift, uden max afgift pr. 1.1.2016"), VAN30M(
																							"M3",
																							"Varevogn 30% afgift, max afgift"), VAN30M2016(
																									"MX",
																									"Varevogn 30% afgift, max afgift pr. 1.1.2016"), VAN50V(
																											"V5",
																											"Varevogn 50% afgift"), VAN50ELECTRIC(
																													"VL",
																													"EL biler, Varevogn 50% afgift"), VAN50HYBRID(
																															"VH",
																															"Hybrid, Varevogn 50% afgift"), VAN50V2016(
																																	"X5",
																																	"Varevogn 50% afgift pr. 1.1.2016"), NO_TAX(
																																			"0 ",
																																			"0% afgift"), MINIBUS(
																																					"60",
																																					"10 personers bus 60% afgift"), MINIBUS2016(
																																							"6X",
																																							"10 personers bus 60% afgift pr. 1.1.2016"), MOTORCYCLE(
																																									"MC",
																																									"Motorcykel 150% afgift"), COACH150(
																																											"PG",
																																											"Personvogn 150% afgift"), CAMPERS150(
																																													"CA",
																																													"Campere 150% afgift");

	private String value;
	private String description;

	private TaxCodeType(final String value, final String description) {
		this.value = value;
		this.description = description;
	}

	public static TaxCodeType getByValue(final String value) {
		if (null == value) {
			throw new NullPointerException("Value cannot be null for this enum type");
		}

		for (final TaxCodeType taxCodeType : TaxCodeType.values()) {
			if (taxCodeType.value.equals(value)) {
				return taxCodeType;
			}
		}

		if (value.length() == 1) {
			// This is a old tax code that needs to be migrated to the new tax
			// code which is 2 characters long
			// The rule is to append a space to the old tax code
			final String newTaxCode = value + " ";
			for (final TaxCodeType taxCodeType : TaxCodeType.values()) {
				if (taxCodeType.value.equals(newTaxCode)) {
					return taxCodeType;
				}
			}
		}

		throw new IllegalArgumentException("Value: '" + value + "' could not be translated into this enum type");
	}

	@Override
	public String toString() {
		return String.format("%s:%S", value, description);
	}
}