package dk.semlerit.autocore.ws.saledocument.core.model;

import lombok.Getter;
import lombok.NonNull;

/**
 * @author extbda
 * @author mi
 * @author lafl
 * @author extthtb
 * 
 * @version 3.0
 * @since 2.6.0
 */
@Getter
public enum UsedVehicleTypeIndicator {
	NOT_SPECIFIED(-1), COACH(0), VAN(1);

	private int value;

	private UsedVehicleTypeIndicator(final int value) {
		this.value = value;
	}

	public static UsedVehicleTypeIndicator getByValue(@NonNull Integer value) {
		for (final UsedVehicleTypeIndicator usedVehicleTypeIndicatorType : UsedVehicleTypeIndicator.values()) {
			if (usedVehicleTypeIndicatorType.value == value.intValue()) {
				return usedVehicleTypeIndicatorType;
			}
		}
		throw new IllegalArgumentException("Value: '" + value + "' could not be translated into this enum type");
	}
}
