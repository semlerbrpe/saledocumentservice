package dk.semlerit.autocore.ws.saledocument.core.model;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class ExternalSalePacketOfferValue implements Serializable {
	private static final long serialVersionUID = 3430182070085005558L;

	private String key;
	private String value;
	private String label;

}