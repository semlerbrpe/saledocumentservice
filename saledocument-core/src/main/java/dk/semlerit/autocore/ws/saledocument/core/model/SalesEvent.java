package dk.semlerit.autocore.ws.saledocument.core.model;

/**
 * Enum provided by Nordania. It represents Sales events.
 * 
 * @author extsip
 * 
 * @version 1.0
 * @since 3.0.0
 */
public enum SalesEvent {
	INKSIGNED, ORDERED, CANCELLED, DELIVERY
}