package dk.semlerit.autocore.ws.saledocument.core.model;

import java.util.Objects;

import lombok.Getter;

/**
 * <p>
 * Indicate the type of value in the cpr/cvr field - indicator is introduced as
 * a mapping to the value returned from CRM
 * </p>
 * 
 * @author extbda
 * @author mi
 * @author lafl
 * @author extthtb
 * 
 * @version 3.0
 * @since 2.6.0
 */
@Getter
public enum CvrCprIndicator {
	NOT_SPECIFIED(-1, ""), CPR(0, "CPR-nr."), CVR(1, "CVR-nr.");

	private int value;
	private String indicator;

	private CvrCprIndicator(final int value, final String indicator) {
		this.value = value;
		this.indicator = indicator;
	}

	/**
	 * <p>
	 * Return CvrCprIndicatorType based on indcator in notes service.
	 * </p>
	 * 
	 * <p>
	 * Dose not throw exceptions
	 * </p>
	 * 
	 * @param indicator
	 *            CPR/CVR Indicator from service backend.
	 * @return Return CvrCprIndicatorType CPR or CVR otherwise return NOT_SPECIFIED
	 */
	public static CvrCprIndicator getByIndicator(final String indicator) {
		if (Objects.isNull(indicator)) {
			return CvrCprIndicator.NOT_SPECIFIED;
		}

		for (final CvrCprIndicator cvrCprIndicatorType : CvrCprIndicator.values()) {
			if (cvrCprIndicatorType.indicator.equals(indicator)) {
				return cvrCprIndicatorType;
			}
		}

		return CvrCprIndicator.NOT_SPECIFIED;
	}

	public static CvrCprIndicator getByValue(final Integer value) {
		if (Objects.isNull(value)) {
			throw new IllegalArgumentException("value cannot be null for this enum type");
		}

		for (final CvrCprIndicator cvrCprIndicatorType : CvrCprIndicator.values()) {
			if (cvrCprIndicatorType.value == value.intValue()) {
				return cvrCprIndicatorType;
			}
		}

		throw new IllegalArgumentException("Value: '" + value + "' could not be translated into this enum type");
	}
}