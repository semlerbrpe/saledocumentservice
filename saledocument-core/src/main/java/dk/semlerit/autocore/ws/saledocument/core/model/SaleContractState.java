package dk.semlerit.autocore.ws.saledocument.core.model;

/**
 * Used when navigating to the sales document portlet
 * 
 * @author extbda
 * 
 * @version 1.0
 * @since 3.0.0
 */
public enum SaleContractState {
	MAKEMODEL,
	DETAILS,
	EQUIPMENT
}
