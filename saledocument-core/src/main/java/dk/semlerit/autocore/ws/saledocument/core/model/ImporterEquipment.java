package dk.semlerit.autocore.ws.saledocument.core.model;

import java.io.Serializable;
import java.math.BigDecimal;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @author extbda
 * @author mi
 * @author lafl
 * @author extthtb
 * 
 * @version 3.0
 * @since 2.6.0
 */
@Getter
@Setter
@Builder
@ToString
public class ImporterEquipment implements Serializable {
	private static final long serialVersionUID = -467400882747038533L;

	private String code;
	private String description;
	private BigDecimal listPriceExclVAT;
	private BigDecimal priceExclVAT;
	private BigDecimal priceInclVAT;

}
