package dk.semlerit.autocore.ws.saledocument.core.model;

import java.io.Serializable;
import java.math.BigDecimal;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class EquipmentAccessory implements Serializable {
	private static final long serialVersionUID = 1683733864755762615L;
	private String partNumber;
	private String accessoryName;
	private BigDecimal priceExcludingVAT;
	private BigDecimal priceIncludingVAT;

}