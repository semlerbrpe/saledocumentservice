package dk.semlerit.autocore.ws.saledocument.core.model;

import lombok.Getter;

/**
 * @author extbda
 * @author mi
 * @author lafl
 * @author extthtb
 * 
 * @version 3.0
 * @since 2.6.0
 */
@Getter
public enum ContractType {
	PRIVATE(1), BUSINESS(2), SCHOOL_CARRIAGE(3);

	private int value;

	private ContractType(final int value) {
		this.value = value;
	}

	public static ContractType getByValue(final int value) {
		for (final ContractType contractTypeEnum : ContractType.values()) {
			if (contractTypeEnum.getValue() == value) {
				return contractTypeEnum;
			}
		}
		throw new IllegalArgumentException("Illegal status value " + value);
	}
}
