package dk.semlerit.autocore.ws.saledocument.core.model;

import lombok.Getter;
import lombok.NonNull;

/**
 * @author extbda
 * @author mi
 * @author lafl
 * @author extthtb
 * 
 * @version 3.0
 * @since 2.6.0
 */
@Getter
public enum GrandTotalPrice {
	NOT_SPECIFIED(-1), OUTSTANDING(0), EXCHANGE(1), CASH(2);

	private int value;

	private GrandTotalPrice(final int value) {
		this.value = value;
	}

	public static GrandTotalPrice getByValue(@NonNull Integer value) {
		for (final GrandTotalPrice grandTotalPriceType : GrandTotalPrice.values()) {
			if (grandTotalPriceType.value == value.intValue()) {
				return grandTotalPriceType;
			}
		}

		throw new IllegalArgumentException("Value: '" + value + "' could not be translated into this enum type");
	}
}