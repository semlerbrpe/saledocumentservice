package dk.semlerit.autocore.ws.saledocument.core.model;

import java.util.Objects;

import lombok.Getter;
import lombok.NonNull;

/**
 * @author extbda
 * @author mi
 * @author lafl
 * @author extthtb
 * 
 * @version 3.0
 * @since 2.6.0
 */
@Getter
public enum UserType {
	NONE(-1, "", ""), PRIMARY_OWNER(2, "owner", "Owner1TO_"), SECONDARY_OWNER(1, "owner2", "Owner2TO_"), PRIMARY_USER(0,
			"user", "User1TO_"), SECONDARY_USER(3, "user2", "User2TO_");

	private int value;
	private String cssValue;
	private String field;

	private UserType(final int value, final String cssValue, final String field) {
		this.value = value;
		this.cssValue = cssValue;
		this.field = field;
	}

	public static UserType getByValue(@NonNull Integer value) {
		for (final UserType userType : UserType.values()) {
			if (userType.value == value.intValue()) {
				return userType;
			}
		}
		throw new IllegalArgumentException("Value: '" + value + "' could not be translated into this enum type");
	}

	public static UserType getByField(@NonNull String field) {
		for (final UserType userType : UserType.values()) {
			if (userType.field.equals(field)) {
				return userType;
			}
		}
		throw new IllegalArgumentException("Field: '" + field + "' could not be translated into this enum type");
	}

	/**
	 * <p>
	 * Translate a CSS value to a UserType
	 * </p>
	 * 
	 * @param cssValue
	 *            css input string
	 * @return User type, if css is blank return NONE.
	 */
	public static UserType getByCssValue(String cssValue) {
		if (Objects.isNull(cssValue) || cssValue.isEmpty()) {
			// If css is null returned value should be none
			return NONE;
		}

		// convert old values to enum
		if ("true".equalsIgnoreCase(cssValue)) {
			return PRIMARY_OWNER;
		}
		if ("false".equalsIgnoreCase(cssValue)) {
			return NONE;
		}

		for (UserType userType : UserType.values()) {
			if (userType.cssValue.equals(cssValue)) {
				return userType;
			}
		}
		throw new IllegalArgumentException("CssValue: '" + cssValue + "' could not be translated into this enum type");
	}
}