package dk.semlerit.autocore.ws.saledocument.core.model;

public enum WinterWheel {
	NOTAPPLICABLE, STEEL, ALU, SWAPTIRES;
}
