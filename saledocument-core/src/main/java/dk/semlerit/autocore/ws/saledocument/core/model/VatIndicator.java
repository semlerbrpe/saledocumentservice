package dk.semlerit.autocore.ws.saledocument.core.model;

import lombok.Getter;
import lombok.NonNull;

/**
 * @author extbda
 * @author mi
 * @author lafl
 * @author extthtb
 * 
 * @version 3.0
 * @since 2.6.0
 */
@Getter
public enum VatIndicator {
	NOT_SPECIFIED(-1), INCLUSIVE_VAT(0), EXCLUSIVE_VAT(1);

	private int value;

	private VatIndicator(final int value) {
		this.value = value;
	}

	public static VatIndicator getByValue(@NonNull Integer value) {
		for (final VatIndicator vatIndicatorType : VatIndicator.values()) {
			if (vatIndicatorType.value == value.intValue()) {
				return vatIndicatorType;
			}
		}
		throw new IllegalArgumentException("Value: '" + value + "' could not be translated into this enum type");
	}
}