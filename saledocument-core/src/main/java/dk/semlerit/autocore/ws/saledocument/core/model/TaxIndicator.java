package dk.semlerit.autocore.ws.saledocument.core.model;

import lombok.Getter;
import lombok.NonNull;

/**
 * @author extbda
 * @author mi
 * @author lafl
 * @author extthtb
 * 
 * @version 3.0
 * @since 2.6.0
 */
@Getter
public enum TaxIndicator {
	NOT_SPECIFIED(-1), INCLUSIVE_TAX(0), EXCLUSIVE_TAX(1);

	private int value;

	private TaxIndicator(final int value) {
		this.value = value;
	}

	public static TaxIndicator getByValue(@NonNull Integer value) {
		for (final TaxIndicator taxIndicatorType : TaxIndicator.values()) {
			if (taxIndicatorType.value == value.intValue()) {
				return taxIndicatorType;
			}
		}

		throw new IllegalArgumentException("Value: '" + value + "' could not be translated into this enum type");
	}
}