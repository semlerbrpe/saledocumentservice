package dk.semlerit.autocore.ws.saledocument.core.model;

import lombok.Getter;

/**
 * @author extbda
 * @author mi
 * @author lafl
 * @author extthtb
 * 
 * @version 3.0
 * @since 2.6.0
 */
@Getter
public enum DocumentType {
	NOT_SPECIFIED("Ukendt"), BUSINESSSALEOFFER("Erhvervstilbud"), SALEOFFER("Tilbud"), SALECONTRACT(
			"Slutseddel"), PRICESIMULATION("Prissimulering");

	private String value;

	private DocumentType(final String value) {
		this.value = value;
	}

	public static DocumentType getByCode(final String code) {
		if (null == code) {
			throw new NullPointerException("Code cannot be null for this enum type");
		}

		for (final DocumentType documentType : DocumentType.values()) {
			if (documentType.value.equals(code)) {
				return documentType;
			}
		}

		throw new IllegalArgumentException("Code: '" + code + "' could not be translated into this enum type");
	}
}