package dk.semlerit.autocore.ws.saledocument.core.model;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @author extbda
 * @author mi
 * @author lafl
 * @author extthtb
 * 
 * @version 3.0
 * @since 2.6.0
 */
@Getter
@Setter
@ToString
public class VersionLog implements Serializable {
	private static final long serialVersionUID = 3925309243289535887L;

	private String version;
	private String createDate;
	private String createUser;
	private String changeDate;
	private String changeUser;
	private String documentID;

}