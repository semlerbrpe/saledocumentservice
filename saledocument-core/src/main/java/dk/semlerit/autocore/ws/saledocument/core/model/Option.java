package dk.semlerit.autocore.ws.saledocument.core.model;

import java.io.Serializable;
import java.math.BigDecimal;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @author extbda
 * @author mi
 * @author lafl
 * @author extthtb
 * 
 * @version 3.0
 * @since 2.6.0
 */
@Getter
@Setter
@Builder
@ToString
public class Option implements Serializable {
	private static final long serialVersionUID = 8974293578664381921L;

	private int optionId;
	private BigDecimal optionPrice;
	private String optionDescription;

}
