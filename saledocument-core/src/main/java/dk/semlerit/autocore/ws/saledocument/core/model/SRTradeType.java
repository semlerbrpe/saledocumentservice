package dk.semlerit.autocore.ws.saledocument.core.model;

import lombok.Getter;
import lombok.NonNull;

/**
 * @author extbda
 * @author mi
 * @author lafl
 * @author extthtb
 * 
 * @version 3.0
 * @since 2.6.0
 */
@Getter
public enum SRTradeType {
	NOT_SPECIFIED(-1), WHOLE_SALE(0), RETAIL_SALE(1), RETAIL_OWN_DEPARTMENT(2);

	private int value;

	private SRTradeType(final int value) {
		this.value = value;
	}

	public static SRTradeType getByValue(@NonNull Integer value) {
		for (final SRTradeType sRTradeType : SRTradeType.values()) {
			if (sRTradeType.value == value.intValue()) {
				return sRTradeType;
			}
		}
		throw new IllegalArgumentException("Value: '" + value + "' could not be translated into this enum type");
	}
}