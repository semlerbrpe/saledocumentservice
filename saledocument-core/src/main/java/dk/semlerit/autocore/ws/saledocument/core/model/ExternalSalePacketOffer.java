package dk.semlerit.autocore.ws.saledocument.core.model;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * <p>
 * This class is a general superclass for external sale packets.
 * </p>
 * <ul>
 * <li>FinanceOffer</li>
 * <li>InsuranceOffer</li>
 * <li>AllInclusiveOffer</li>
 * <li>LeasingOffer</li>
 * </ul>
 * <p>
 * Any specialized properties should be sub classed from this common class.<br/>
 * Please notice that the static inner class for value collection, can be sub
 * classed as well if needed.
 * </p>
 * 
 * @author extbda
 * @author mi
 * @author lafl
 * @author extthtb
 * 
 * @version 3.0
 * @since 2.6.0
 */
@Getter
@Setter
@ToString
public class ExternalSalePacketOffer implements Serializable {
	private static final long serialVersionUID = 7232537150765328133L;

	private String id;
	private String providerId;
	private String pdf;
	private List<ExternalSalePacketOfferValue> externalSalePacketOfferValues = new LinkedList<ExternalSalePacketOfferValue>();

}