package dk.semlerit.autocore.ws.saledocument.core.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 
 * 
 * @author edbbrpe
 * @version 1.0
 * @since 3.0.0
 */
@Getter
@Setter
@Builder
@ToString
public class SalesChannel implements Serializable {
	private static final long serialVersionUID = -2813621758861705268L;

	private Integer id;
	private int typeId;
	private String name;
	private BusinessType businessType;
	private List<PaymentType> paymentTypes;
	private List<String> prNumbers = new ArrayList<String>();
	private Calendar activeFrom;
	private Calendar activeTo;
}
