package dk.semlerit.autocore.ws.saledocument.core.model;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @author edbbrpe
 *
 */
@Getter
@Setter
@ToString
public class CustomerRelationManagement implements Serializable {
	private static final long serialVersionUID = 2329996083188247612L;

	private Integer initiative;
	private Integer contactMethod;
	private Integer activityTime;
	private String commentCurrent;
	private String commentNext;
	private boolean newOwnBrand;
	private boolean usedOwnBrand;
	private boolean foreignBrand;
	private Boolean van;
	private String caseId;

}
