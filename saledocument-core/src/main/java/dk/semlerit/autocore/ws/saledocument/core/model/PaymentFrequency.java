package dk.semlerit.autocore.ws.saledocument.core.model;

import java.io.Serializable;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @author extbda
 * @author mi
 * @author lafl
 * @author extthtb
 * 
 * @version 3.0
 * @since 2.6.0
 */
@Getter
@Setter
@Builder
@ToString
public class PaymentFrequency implements Serializable {
	private static final long serialVersionUID = 6410071003076019139L;

	private int id;
	private int frequency;
	private String frequencyDescription;

}
