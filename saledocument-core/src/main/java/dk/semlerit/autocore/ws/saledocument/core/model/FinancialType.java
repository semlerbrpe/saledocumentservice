package dk.semlerit.autocore.ws.saledocument.core.model;

/**
 * We support 3 types of financing. Cash, loan or leasing. Loan/Leasing products
 * are supplied by Nordania.
 * 
 * @author extbda
 * 
 * @version 1.0
 * @since 3.0.0
 */
public enum FinancialType {
	CASH, LOAN, LEASING;
}
