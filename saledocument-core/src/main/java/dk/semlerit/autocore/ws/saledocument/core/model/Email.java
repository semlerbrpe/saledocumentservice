package dk.semlerit.autocore.ws.saledocument.core.model;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @author edbbrpe
 *
 */
@Getter
@Setter
@ToString
public class Email implements Serializable {
	private static final long serialVersionUID = 2239408324650653908L;

	private String sequenceNumber;
	private String email;
	private String emailType;

}
