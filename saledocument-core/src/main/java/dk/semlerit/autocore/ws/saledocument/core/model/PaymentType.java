package dk.semlerit.autocore.ws.saledocument.core.model;

/**
 * A enum representing the legal values in the ModelHierarchy
 * 
 * @author extbda
 * @since 3.0.0
 * @version 1.0
 */
public enum PaymentType {
	PRIVATEBUYING, COMMERCIALBUYING, PRIVATEFINANCING, COMMERCIALFINANCING, PRIVATELEASING, COMMERCIALLEASING;
}