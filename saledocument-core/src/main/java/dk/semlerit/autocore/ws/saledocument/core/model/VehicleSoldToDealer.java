package dk.semlerit.autocore.ws.saledocument.core.model;

import java.io.Serializable;
import java.math.BigDecimal;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @author extbda
 * @author mi
 * @author lafl
 * @author extthtb
 * 
 * @version 3.1
 * @since 2.6.0
 */
@Getter
@Setter
@ToString
public class VehicleSoldToDealer implements Serializable {
	private static final long serialVersionUID = -1891845054074151570L;

	private Integer make;
	private String model;
	private String identificationNumber;
	private String firstRegistrationDate;
	private String modelYear;
	private String productionYear;
	private String color;
	private String licensePlate;
	private String equipment;
	private String extraEquipment;
	private String inspectionDate;
	private String kilometers;
	private Boolean kilometerWarranty;
	private String timingBeltChangeDate;
	private String corrosionWarranty;
	private Integer registrationTaxRule;
	private Integer usage;
	private Integer involvedInAccident;
	private String accidentDescription;
	private String additionalComments;
	private String outstandingDebtTo;
	private Integer damagePaidBy;
	private String damageScope;
	private BigDecimal damageInstallmentAmount;
	private Integer timingBeltChangeKilometers;
	private Integer serviceBookMaintained;
	// Only used by SaleContract
	private SemlerRetailVehicleTrade srVehicleTrade;

}