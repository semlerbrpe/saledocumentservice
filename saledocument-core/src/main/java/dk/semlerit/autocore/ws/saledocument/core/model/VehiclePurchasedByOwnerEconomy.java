/**
 * 
 */
package dk.semlerit.autocore.ws.saledocument.core.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Calendar;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @author edbbrpe
 *
 */
@Getter
@Setter
@Builder
@ToString
public class VehiclePurchasedByOwnerEconomy implements Serializable {
	private static final long serialVersionUID = -8611802957774826956L;

	private Integer priceIncludingRegistrationTaxLabel;
	private BigDecimal priceIncludingRegistrationTax;
	private BigDecimal equipmentProvidedByFactory;
	private BigDecimal equipmentProvidedByDealer;
	private String field1;
	private BigDecimal price1;
	private String field2;
	private BigDecimal price2;
	private String field3;
	private BigDecimal price3;
	private BigDecimal deliveryExpenses;
	private BigDecimal licenseTagFee;
	private BigDecimal vat;
	private Integer priceVatIndicator;
	private BigDecimal cashOnDeliveryPrice;
	private BigDecimal vehicleSoldToDealerTotalPrice;
	private Integer totalPriceType;
	private BigDecimal totalPrice;
	private BigDecimal price;
	private BigDecimal priceInclVAT;
	private String remainingAmountInstitute;
	private BigDecimal remainingAmountMonths;
	private BigDecimal remainingAmountMonthlyPayment;
	private BigDecimal remainingAmountAnnualInterest;
	private String cashOnDeliveryLabel;
	private String priceCode;
	private String taxCode;
	private Calendar calculationDate;
	private PriceConfiguration priceConfiguration;
	private BigDecimal totalVehiclePriceExcludingVat;
	private BigDecimal totalVehiclePriceIncludingVat;
	private BigDecimal totalVehiclePriceVat;
	private BigDecimal payment;
	private BigDecimal totalPriceExcludingTaxIncludingVat;
	private BigDecimal totalPriceExcludingTaxExcludingVat;

}
