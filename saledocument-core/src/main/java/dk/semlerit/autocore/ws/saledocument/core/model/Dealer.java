package dk.semlerit.autocore.ws.saledocument.core.model;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @author extbda
 * @author mi
 * @author lafl
 * @author extthtb
 * 
 * @version 3.0
 * @since 2.6.0
 */
@Getter
@Setter
@ToString
public class Dealer implements Serializable {
	private static final long serialVersionUID = 4426724488097819485L;
	private String name;
	private String address;
	private String zip;
	private String city;
	private String phoneNumber;
	private String faxNumber;
	private String bankRegistrationNumber;
	private String bankAccountNumber;
	private String swift;
	private String iban;
	private String cvrNumber;
	private String email;

}