package dk.semlerit.autocore.ws.saledocument.core.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @author extbda
 * @author mi
 * @author lafl
 * @author extthtb
 * 
 * @version 3.0
 * @since 2.6.0
 */
@Getter
@Setter
@ToString
public class DocumentCustomer extends Customer {
	private static final long serialVersionUID = -7624464849070663436L;

	private CvrCprIndicator cvrCprIndicator = CvrCprIndicator.NOT_SPECIFIED;
	private String socialSecurityOrCVRNumber;
	private String phoneNumber;
	private String mobileNumber;
	private String mailAddress;
	private String extraInformation;
	private String identificationIndicator;

}