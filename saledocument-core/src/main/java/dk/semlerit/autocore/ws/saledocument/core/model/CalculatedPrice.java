package dk.semlerit.autocore.ws.saledocument.core.model;

import java.io.Serializable;
import java.math.BigDecimal;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @author extbda
 * @author mi
 * @author lafl
 * @author extthtb
 * 
 * @version 3.0
 * @since 2.6.0
 */
@Getter
@Setter
@Builder
@ToString
public class CalculatedPrice implements Serializable {
	private static final long serialVersionUID = 6694965154494363726L;
	private BigDecimal pricePerPayment;
	private BigDecimal contractPricePerKm;
	private BigDecimal totalPrice;
	private BigDecimal additionalKilometersPrice;
	private BigDecimal deductibleKilometersPrice;
	private boolean vatIncluded;

}
