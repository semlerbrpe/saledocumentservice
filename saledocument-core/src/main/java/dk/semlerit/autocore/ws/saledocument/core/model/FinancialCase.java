package dk.semlerit.autocore.ws.saledocument.core.model;

import java.io.Serializable;
import java.util.Date;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Object used to hold Nordania information. These information is persisted in
 * NG. Other information is Document is persisted in Notes.
 * 
 * @author extbda
 * 
 * @version 1.0
 * @since 3.0.0
 */
@Getter
@Setter
@ToString
public class FinancialCase implements Serializable {
	private static final long serialVersionUID = 1492623522809931026L;

	private String financialCaseId;
	private String sessionId;
	private String userFriendlyCaseId;
	private Date createDate;
	private Date updateDate;
	private String customerDataId;
	private int version;
	private Integer extVersion;
	private Long calculationId;
	private Long existingCalculationId;
	private FinancialType financialType = FinancialType.CASH;
	private FinancialCaseStatus financialCaseStatus;

}
