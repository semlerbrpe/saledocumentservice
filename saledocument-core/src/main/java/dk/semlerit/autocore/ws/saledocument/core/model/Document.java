package dk.semlerit.autocore.ws.saledocument.core.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import lombok.Builder;
import lombok.Builder.Default;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @author extbda
 * @author mi
 * @author lafl
 * @author extthtb
 * 
 * @version 3.0
 * @since 2.6.0
 */
@Getter
@Setter
@Builder
@ToString
public class Document implements Serializable {
	private static final long serialVersionUID = 2890111280176264148L;

	private String id;
	private String idSourceDocument;
	private String fullNotesDocumentURL;
	private String documentOwner;
	private String parentId;
	private String previousVersionDocumentID;
	@Default
	private DocumentType documentType = DocumentType.NOT_SPECIFIED;
	private DocumentStatus status;
	private String statusCaseNumber;
	private boolean inHierarchy;
	@Default
	private boolean createdByService = true;
	private Boolean acceptedMarketing;
	private boolean businessSaleOfferCopy;
	private boolean saleOfferCopy;
	private Boolean vehicleOwnershipPOA;
	private DocumentStatus originalStatus;
	private Integer quantity;
	private String saleOrderId;
	private String vehicleOrderId;
	private boolean archived;
	private String owner;
	private String sellerName;
	private String sellerEmail;
	private String buyerName;
	private String additionalAgreementComments;
	private String warrantyConditions;
	private String date;
	@Default
	private GrandTotalPrice grandTotalPriceType = GrandTotalPrice.NOT_SPECIFIED;

	private String remainingAmountInstitute;
	private BigDecimal remainingAmountMonths;
	private BigDecimal remainingAmountMonthlyPayment;
	private BigDecimal remainingAmountAnnualInterest;

	// Financial Case
	private FinancialCase financialCase;

	// Customers
	private DocumentCustomer owner1;
	private boolean owner1Updated;
	private DocumentCustomer owner2;
	private boolean owner2Updated;
	private DocumentCustomer user1;
	private boolean user1Updated;
	private DocumentCustomer user2;
	private boolean user2Updated;

	// Vehicle purchased by owner(s)
	private VehiclePurchasedByOwner vehiclePurchasedByOwner;
	private boolean purchasedByOwnerEconomyDraftPresent;
	private boolean vehicleDeleted;

	// Vehicle sold to dealer
	private Dealer dealer;
	private VehicleSoldToDealer vehicleSoldToDealer;
	private boolean soldToDealerEconomyDraftPresent;

	// Calculation configuration and result.
	private PriceConfiguration priceConfiguration;
	private Economy economy;
	@Default
	private ArrayList<HistoryLog> historyLogs = new ArrayList<HistoryLog>();
	@Default
	private ArrayList<EmailLog> emailLogs = new ArrayList<EmailLog>();
	@Default
	private ArrayList<OfferLog> offerLogs = new ArrayList<OfferLog>();
	@Default
	private ArrayList<VersionLog> versionLogs = new ArrayList<VersionLog>();
	private CustomerRelationManagement customerRelationManagement;
	private Calendar migrationDate;
	private Calendar lastModifiedDate;
	private String version;
	private Date lastUpdated;
	private String deliveryDocumentId;
	private String title;

}