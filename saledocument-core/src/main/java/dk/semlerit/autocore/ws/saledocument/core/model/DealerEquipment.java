package dk.semlerit.autocore.ws.saledocument.core.model;

import java.io.Serializable;
import java.math.BigDecimal;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class DealerEquipment implements Serializable {
	private static final long serialVersionUID = 17856613107717761L;

	private String description;
	private BigDecimal priceExcludingVAT;
	private BigDecimal priceIncludingVAT;

}