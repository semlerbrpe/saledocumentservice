package dk.semlerit.autocore.ws.saledocument.core.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;

import lombok.Builder;
import lombok.Builder.Default;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * <p>
 * This class is a general superclass for equipment.
 * </p>
 * <ul>
 * <li>ImporterDefinedEquipment</li>
 * <li>DealerEquipment</li>
 * <li>EquipmentSpecification</li>
 * </ul>
 * <p>
 * Any specialized properties should be sub classed from this common class.<br/>
 * Please notice that the static inner class for accessories collection, can be
 * sub classed as well if needed.
 * </p>
 * 
 * @author extbda
 * @author mi
 * @author lafl
 * @author extthtb
 * 
 * @version 3.0
 * @since 2.6.0
 */
@Getter
@Setter
@Builder
@ToString
public class Equipment implements Serializable {
	private static final long serialVersionUID = -8154517122926841303L;

	private String code;
	private String name;
	private String description;
	private String partNumber;
	@Default
	private EquipmentType type = EquipmentType.NOT_SPECIFIED;
	private BigDecimal priceIncludingVAT;
	private BigDecimal listPriceExcludingVAT;
	private BigDecimal priceExcludingVAT;
	private BigDecimal tax;
	private BigDecimal accessoryExcludingVAT;
	private BigDecimal accessoryIncludingVAT;
	private BigDecimal hoursRequiredForFitting;
	private BigDecimal fittingExcludingVAT;
	private BigDecimal fittingIncludingVAT;
	private BigDecimal totalExcludingVAT;
	private BigDecimal totalIncludingVAT;
	private String thumbnailURL;
	private String largeImageURL;
	@Default
	private List<EquipmentAccessory> equipmentAccessories = new LinkedList<EquipmentAccessory>();
	private String partOfPacket;
	private String dependencyCssClasses;
	private String removeToolTip;
	@Default
	private boolean canBeRemoved = true;

}