package dk.semlerit.autocore.ws.saledocument.core.model;

import java.util.Arrays;

import lombok.Getter;
import lombok.NonNull;

/**
 * @author extbda
 * @author mi
 * @author lafl
 * @author extals
 * @author extthtb
 * 
 * @version 3.0
 * @since 2.6.0
 */
@Getter
public enum VehicleMake {
	NOT_SPECIFIED(""), VW("SP.BRAND.VW"), VWVAN("SP.BRAND.VWVAN"), AUDI("SP.BRAND.AUDI"), SKODA("SP.BRAND.SKODA"), SEAT(
			"SP.BRAND.SEAT"), PORSCHE("SP.BRAND.PORSCHE"), NEW_OWN_BRAND("NEW_OWN_BRAND"), USED_OWN_BRAND(
					"USED_OWN_BRAND"), FOREIGN_BRAND("FOREIGN_BRAND"), NEW_FOREIGN_BRAND("NEW_FOREIGN_BRAND");

	private String value;

	private VehicleMake(final String value) {
		this.value = value;
	}

	public static VehicleMake getByValue(@NonNull String value) {
		for (final VehicleMake vehicleMakeType : VehicleMake.values()) {
			if (vehicleMakeType.getValue().equals(value)) {
				return vehicleMakeType;
			}
		}
		throw new IllegalArgumentException("Value: '" + value + "' could not be translated into this enum type");
	}

	public static boolean isNewVehicleMake(@NonNull VehicleMake vehicleMakeType) {
		return Arrays.stream(new VehicleMake[] { AUDI, SEAT, SKODA, VW, VWVAN, PORSCHE })
				.anyMatch(v -> (vehicleMakeType.equals(v)));
	}

	public static boolean isNewVehicleMakeStockIncluded(@NonNull VehicleMake vehicleMakeType) {
		return Arrays.stream(new VehicleMake[] { AUDI, SEAT, SKODA, VW, VWVAN, PORSCHE, NEW_OWN_BRAND })
				.anyMatch(v -> (vehicleMakeType.equals(v)));
	}

	public static boolean isUsedVehicleMake(@NonNull VehicleMake vehicleMakeType) {
		return Arrays.stream(new VehicleMake[] { USED_OWN_BRAND, FOREIGN_BRAND })
				.anyMatch(v -> (vehicleMakeType.equals(v)));
	}

	public static boolean isVehicleInStock(@NonNull VehicleMake vehicleMakeType) {
		return Arrays.stream(new VehicleMake[] { USED_OWN_BRAND, NEW_OWN_BRAND, FOREIGN_BRAND })
				.anyMatch(v -> (vehicleMakeType.equals(v)));
	}
}