package dk.semlerit.autocore.ws.saledocument.core.model;

import java.io.Serializable;
import java.math.BigDecimal;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @author extbda
 * @author mi
 * @author lafl
 * @author extthtb
 * 
 * @version 3.0
 * @since 2.6.0
 */
@Getter
@Setter
@Builder
@ToString
public class ExtendedWarranty implements Serializable {
	private static final long serialVersionUID = 7203237042893892891L;

	private String id;
	private String description;
	private BigDecimal priceIncludingVAT;
	private BigDecimal priceExcludingVAT;
	private BigDecimal VAT;
	private String pdf;

}
