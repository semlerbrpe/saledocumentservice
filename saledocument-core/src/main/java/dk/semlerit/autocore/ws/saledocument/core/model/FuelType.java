package dk.semlerit.autocore.ws.saledocument.core.model;

/**
 * Enum provided by Nordania.
 * 
 * @author extbda
 * 
 * @version 1.0
 * @since 3.0.0
 */
public enum FuelType {
	PETROL, DIESEL, ELECTRIC, AUTOGAS;
}
