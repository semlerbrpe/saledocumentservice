package dk.semlerit.autocore.ws.saledocument.core.model;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @author edbbrpe
 *
 */
@Getter
@Setter
@ToString
public class Phone implements Serializable {
	private static final long serialVersionUID = -8745875111090348629L;

	private String sequenceNumber;
	private String phoneType;
	private String phoneNumber;

}
