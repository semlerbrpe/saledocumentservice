package dk.semlerit.autocore.ws.saledocument.core.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Customer, used in Create, Read and Update customer service.
 * 
 * <p>
 * 1.1: Initialize list variables
 * </p>
 * 
 * @author extbda
 * @author mi
 * @author lafl
 * @author extthtb
 * 
 * @version 3.0
 * @since 2.6.0
 */
@Getter
@Setter
@ToString
public class Customer implements Serializable {
	private static final long serialVersionUID = 3539581109150975012L;
	private String customerId;
	private String firstName;
	private String lastName;
	private String address1;
	private String address2;
	private String zipCode;
	private String coName;
	private String town;
	private String cpr;
	private String cvr;
	private String country;
	private String customerType;
	private String customerTopic;
	private String customerMark;
	private String customerSatisfactionSurvey;
	private Boolean taxCode;
	private String dead;
	private List<Phone> phone = new ArrayList<Phone>();
	private List<Email> email = new ArrayList<Email>();
	private String serviceLetter;
	private String advertisement;
	private Integer audiMagazine;
	private Integer bilSnakMagazine;
	private Integer skodaMagazine;
	private String belong;
	private String acceptEAN;
	private String eanNumber;
	private String robinson;
	private Integer discountCodeWorkshop;
	private Integer discountCodeDisksale;
	private Boolean environmentCost;
	private Boolean otherMaterials;
	private Integer numberOfCopies;

}