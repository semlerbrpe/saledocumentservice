package dk.semlerit.autocore.ws.saledocument.core.model;

import lombok.Getter;
import lombok.NonNull;

/**
 * @author extbda
 * @author mi
 * @author lafl
 * @author extthtb
 * 
 * @version 3.0
 * @since 2.6.0
 */
@Getter
public enum SRDepartmentNumber {
	NOT_SPECIFIED(0), USED_VW_FINANCE_DEPARTMENT(210), USED_VWVAN_FINANCE_DEPARTMENT(212), USED_AUDI_FINANCE_DEPARTMENT(
			213), USED_SEAT_FINANCE_DEPARTMENT(
					215), USED_SKODA_FINANCE_DEPARTMENT(217), USED_PORSCHE_FINANCE_DEPARTMENT(211);

	private int value;

	private SRDepartmentNumber(final int value) {
		this.value = value;
	}

	public static SRDepartmentNumber getByValue(@NonNull Integer value) {
		for (final SRDepartmentNumber sRDepartmentNumberType : SRDepartmentNumber.values()) {
			if (sRDepartmentNumberType.value == value.intValue()) {
				return sRDepartmentNumberType;
			}
		}
		throw new IllegalArgumentException("Value: '" + value + "' could not be translated into this enum type");
	}
}
