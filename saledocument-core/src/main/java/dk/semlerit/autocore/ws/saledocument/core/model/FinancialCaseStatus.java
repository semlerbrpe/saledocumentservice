package dk.semlerit.autocore.ws.saledocument.core.model;

/**
 * Enum provided by Nordania. It represents common states between Nordania and
 * Semler
 * 
 * @author extbda
 * 
 * @version 1.0
 * @since 3.0.0
 */
public enum FinancialCaseStatus {
	REQUESTED, REPLIED, APPLIED, MANUAL, GRANTED, DENIED, OFFERED, EXPIRED, INKSIGNED, DIGSIGNED, ORDERED, CANCELLED, ACTIVATED, ERROR
}