package dk.semlerit.autocore.ws.saledocument.core.model;

import java.io.Serializable;
import java.math.BigDecimal;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @author extbda
 * @author mi
 * @author lafl
 * @author extthtb
 * 
 * @version 3.2
 * @since 2.6.0
 */
@Getter
@Setter
@Builder
@ToString
public final class PriceConfiguration implements Serializable {
	private static final long serialVersionUID = 8925434174084223403L;

	private BigDecimal carPriceIncludingVAT;
	private BigDecimal carPriceExcludingVAT;
	private String colorCode;
	private String colorCodeLabel;
	private String interiorCode;
	private String interiorCodeLabel;
	private BigDecimal customerDiscountAmount;
	private BigDecimal dealerMargin;
	private BigDecimal discountAmount;
	private BigDecimal additionalDiscountAmount;
	private String campaignCodes;
	private BigDecimal maximumDiscount;
	private BigDecimal minimumStandardPrice;
	private BigDecimal totalPriceExcludingVAT;
	private BigDecimal totalPriceIncludingVAT;
	private BigDecimal importerEquipmentTotalPriceExcludingVAT;
	private BigDecimal importerEquipmentTotalPriceIncludingVAT;
	private BigDecimal importerEquipmentTotalListPriceExcludingVAT;
	private BigDecimal dealerEquipmentTotalPriceExcludingVAT;
	private BigDecimal dealerEquipmentTotalPriceIncludingVAT;
	private BigDecimal additionalEquipmentTotalPriceExcludingVAT;
	private BigDecimal additionalEquipmentTotalPriceIncludingVAT;
	private BigDecimal standardEquipmentTotalPriceExcludingVAT;
	private BigDecimal standardEquipmentTotalPriceIncludingVAT;
	private BigDecimal totalTax;
	private BigDecimal totalVAT;
	private BigDecimal taxExclRegulations;
	private BigDecimal fuelRegulation;
	private BigDecimal securityEquipmentDIS;
	private Integer usedVehicleTypeIndicator;

}
