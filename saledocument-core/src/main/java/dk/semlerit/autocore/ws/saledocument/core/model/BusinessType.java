package dk.semlerit.autocore.ws.saledocument.core.model;

public enum BusinessType {
	PRIVATE,
	BUSINESS;
}
