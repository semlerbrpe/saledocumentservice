package dk.semlerit.autocore.ws.saledocument.core.model;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

import lombok.Builder;
import lombok.Builder.Default;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @author extbda
 * @author mi
 * @author lafl
 * @author extthtb
 * 
 * @version 3.0
 * @since 2.6.0
 */
@Getter
@Setter
@Builder
@ToString
public class FinanceOffer implements Serializable {
	private static final long serialVersionUID = 4740902287354126436L;

	private String id;
	private String providerId;
	private String pdfPDF;
	@Default
	private List<FinanceValue> financeValues = new LinkedList<FinanceValue>();

}
