package dk.semlerit.autocore.ws.saledocument.core.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @author extbda
 * @author mi
 * @author lafl
 * @author extthtb
 * 
 * @version 3.0
 * @since 2.6.0
 */
@Getter
@Setter
@Builder
@ToString
public class ServiceContract implements Serializable {
	private static final long serialVersionUID = 6634946333001385895L;

	private int priceModelId;
	private PaymentFrequency paymentFrequency;
	private int kilometersPerYear;
	private int kilometersPerMonth;
	private ContractType contractType;
	private int contractDuration;
	private int allowedKilometers;
	private BigDecimal dealerDiscountExclVAT;
	private List<Option> options = new ArrayList<Option>();
	private CalculatedPrice basicCalculatedPrice;
}
