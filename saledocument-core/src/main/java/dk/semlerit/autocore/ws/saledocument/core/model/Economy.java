package dk.semlerit.autocore.ws.saledocument.core.model;

import java.io.Serializable;
import java.math.BigDecimal;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @author extbda
 * @author mi
 * @author lafl
 * @author extthtb
 * 
 * @version 3.0
 * @since 2.6.0
 */
@Getter
@Setter
@ToString
public class Economy implements Serializable {
	private static final long serialVersionUID = 5681598820168430721L;

	private BigDecimal dealerPrice;
	private BigDecimal dealerMargin;
	private BigDecimal dealerMarginExclEquipment;
	private BigDecimal minimumStandardPriceTotal;
	private BigDecimal standardPriceRegulation;

	private BigDecimal salesPriceExcludingVat;
	private BigDecimal salesPriceIncludingVat;
	private BigDecimal salesPriceExcludingTaxExcludingVat;
	private BigDecimal salesPriceExcludingTaxIncludingVat;
	private BigDecimal salesPriceVat;
	private BigDecimal salesPriceTax;

	private BigDecimal totalPriceIncludingVat;
	private BigDecimal totalPriceExcludingVat;
	private BigDecimal totalPriceExcludingTaxIncludingVat;
	private BigDecimal totalPriceExcludingTaxExcludingVat;

	private BigDecimal totalVehiclePriceTax;
	private BigDecimal totalVehiclePriceVat;
	private BigDecimal totalVehiclePriceExcludingVat;
	private BigDecimal totalVehiclePriceIncludingVat;
	private BigDecimal totalVehiclePriceExcludingTaxExcludingVat;
	private BigDecimal totalVehiclePriceExcludingTaxIncludingVat;

	private BigDecimal vehicleSoldToDealerPrice;
	private BigDecimal vehicleSoldToDealerTotalPrice;

	private BigDecimal customerDiscount;
	private BigDecimal dealerMaxDiscount;
	private BigDecimal maxTax;
	private BigDecimal totalTax;
	private BigDecimal totalVat;
	private BigDecimal securityEquipmentDiscount;
	private BigDecimal fuelRegulation;
	private BigDecimal taxExcludingRegulations;
	private BigDecimal taxExcludingRegulationsExclEquipment;
	private BigDecimal standardEquipmentTotalPriceIncludingVat;
	private BigDecimal standardEquipmentTotalPriceExcludingVat;

	private BigDecimal additionalEquipmentTotalPriceIncludingVat;
	private BigDecimal additionalEquipmentTotalPriceExcludingVat;

	private BigDecimal importerDefinedDealerEquipmentTotalPriceIncludingVat;
	private BigDecimal importerDefinedDealerEquipmentTotalPriceExcludingVat;
	private BigDecimal importerDefinedDealerEquipmentTotalListPriceExcludingVat;

	private BigDecimal dealerEquipmentTotalPriceIncludingVat;
	private BigDecimal dealerEquipmentTotalPriceExcludingVat;

	private BigDecimal equipmentProvidedByFactoryTotalPrice;
	private BigDecimal equipmentProvidedByFactoryTotalTax;
	private BigDecimal equipmentProvidedByFactoryTotalVat;
	private BigDecimal equipmentProvidedByFactoryExcludingTaxExcludingVat;
	private BigDecimal equipmentProvidedByFactoryExcludingTaxIncludingVat;

	private BigDecimal equipmentProvidedByDealerTotalPrice;

	private BigDecimal grandTotalPrice;
	private BigDecimal regulation;

	private BigDecimal fuelRegulationModel;

}