package dk.semlerit.autocore.ws.saledocument.core.model;

import lombok.Getter;
import lombok.NonNull;

/**
 * @author extbda
 * @author mi
 * @author lafl
 * @author extthtb
 * 
 * @version 3.0
 * @since 2.6.0
 */
@Getter
public enum DeliveryExpenseTypeCode {
	NOT_SPECIFIED(-1, -1), COACH(0, 0), // Car
	VAN_EXCL_VAT_30(1, 2), VAN_EXCL_VAT_50(2, 7), VAN_INCL_VAT_30(3, 1), VAN_INCL_VAT_50(4, 6), TAXI(5,
			4), EXPORT_CAR(6, 5), MOTOR_HOME(7, 3);

	private int index;
	private int value;

	private DeliveryExpenseTypeCode(final int index, final int value) {
		this.index = index;
		this.value = value;
	}

	public static DeliveryExpenseTypeCode getByIndex(@NonNull final Integer index) {
		for (final DeliveryExpenseTypeCode deliveryExpenseTypeCodeType : DeliveryExpenseTypeCode.values()) {
			if (deliveryExpenseTypeCodeType.index == index.intValue()) {
				return deliveryExpenseTypeCodeType;
			}
		}
		throw new IllegalArgumentException("Index: '" + index + "' could not be translated into this enum type");
	}

	public static DeliveryExpenseTypeCode getByValue(@NonNull final Integer value) {
		for (final DeliveryExpenseTypeCode deliveryExpenseTypeCodeType : DeliveryExpenseTypeCode.values()) {
			if (deliveryExpenseTypeCodeType.value == value.intValue()) {
				return deliveryExpenseTypeCodeType;
			}
		}
		throw new IllegalArgumentException("Value: '" + value + "' could not be translated into this enum type");
	}
}