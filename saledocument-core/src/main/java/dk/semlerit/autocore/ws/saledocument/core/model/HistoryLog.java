package dk.semlerit.autocore.ws.saledocument.core.model;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @author extbda
 * @author mi
 * @author lafl
 * @author extthtb
 * 
 * @version 3.0
 * @since 2.6.0
 */
@Getter
@Setter
@ToString
public class HistoryLog implements Serializable {
	private static final long serialVersionUID = 1965368294588090195L;

	private String changed;
	private String by;
	private DocumentType documentType;
	private String status;

}