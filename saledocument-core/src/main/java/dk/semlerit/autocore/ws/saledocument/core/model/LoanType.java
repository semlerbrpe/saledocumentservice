package dk.semlerit.autocore.ws.saledocument.core.model;

/**
 * Enum provided by Nordania.
 * 
 * @author extsip
 * 
 * @version 1.0
 * @since 3.0.0
 */
public enum LoanType {
	HIREPURCHASE, VENDORLOAN, DBLOANWITHVEHICLETITLE, VENDOREMPLOYEELOAN
}
