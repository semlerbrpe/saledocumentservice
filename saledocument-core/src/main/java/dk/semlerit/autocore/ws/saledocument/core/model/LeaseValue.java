package dk.semlerit.autocore.ws.saledocument.core.model;

import java.io.Serializable;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @author extbda
 * @author mi
 * @author lafl
 * @author extthtb
 * 
 * @version 3.0
 * @since 2.6.0
 */
@Getter
@Setter
@Builder
@ToString
public class LeaseValue implements Serializable {
	private static final long serialVersionUID = -8422856541858460873L;

	private String key;
	private String value;
	private String label;

}
