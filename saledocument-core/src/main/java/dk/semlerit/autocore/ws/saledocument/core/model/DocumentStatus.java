package dk.semlerit.autocore.ws.saledocument.core.model;

/**
 * This type represent the status of the Document.
 * 
 * <p>
 * 0: igang 1: annulleret 2: underskrevet 3: afsluttet
 * </p>
 * 
 * @author extbda
 * @author mi
 * @author lafl
 * @author extthtb
 * 
 * @version 3.0
 * @since 2.6.0
 */
public enum DocumentStatus {
	IN_PROGRESS(0), CANCELLED(1), SIGNED(2), COMPLETED(3);

	private int value;

	private DocumentStatus(final int value) {
		this.value = value;
	}

	public int getValue() {
		return value;
	}

	public static DocumentStatus getByValue(final Integer value) {
		if (null == value) {
			throw new NullPointerException("value cannot be null for this enum type");
		}

		for (final DocumentStatus documentStatusType : DocumentStatus.values()) {
			if (documentStatusType.value == value.intValue()) {
				return documentStatusType;
			}
		}

		throw new IllegalArgumentException("Value: '" + value + "' could not be translated into this enum type");
	}
}
