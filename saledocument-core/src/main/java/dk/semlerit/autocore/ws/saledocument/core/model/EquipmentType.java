package dk.semlerit.autocore.ws.saledocument.core.model;

/**
 *
 */
public enum EquipmentType {
	NOT_SPECIFIED,
	STANDARD_EQUIPMENT,
	STANDARD_EQUIPMENT_DESELECTED,
	ADDITIONAL_EQUIPMENT,
	IMPORTER_DEFINED_DEALER_EQUIPMENT,
	DEALER_EQUIPMENT;
}