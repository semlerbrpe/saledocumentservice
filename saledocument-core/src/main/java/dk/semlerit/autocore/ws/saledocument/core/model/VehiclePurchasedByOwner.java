package dk.semlerit.autocore.ws.saledocument.core.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import lombok.Builder;
import lombok.Builder.Default;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @author extbda
 * @author mi
 * @author lafl
 * @author extthtb
 * 
 * @version 3.3
 * @since 2.6.0
 */
@Getter
@Setter
@Builder
@ToString
public class VehiclePurchasedByOwner implements Serializable {
	private static final long serialVersionUID = -4003682506176669307L;

	private String brandCode;
	private VehicleMake make;
	private String model;
	private Integer modelYear;
	private String modelText;
	private String productionYear;
	private String variantDocumentId;
	private String variant;
	private String uniqueModelId;
	private String vehicleId;
	private String scheduleCode;
	private String vin;
	private Boolean euNumberPlate;
	private Boolean kilometerWarranty;
	private Boolean involvedInAccident;
	private String vehicleType;
	private String insuranceCompany;
	private String insurancePolicyNumber;
	private String firstRegistrationDate;
	private String licensePlate;
	private Integer doors;
	private String fuelType;
	private BigDecimal kilometersPerLiter;
	private BigDecimal wltpFuelConsumption;
	private BigDecimal carPriceIncludingVat;
	private BigDecimal carPriceExcludingVat;
	private BigDecimal totalPriceIncludingVat;
	private BigDecimal totalPriceExcludingVat;
	private Integer vanTaxRate;
	private String typeCode;
	@Default
	private String[] prNumbers = new String[0];
	private ContactInformation contactInformation;
	private String factoryEquipment;
	@Default
	private List<Equipment> standardEquipments = new ArrayList<Equipment>();
	@Default
	private List<Equipment> standardEquipmentDeselecteds = new ArrayList<Equipment>();
	@Default
	private List<Equipment> additionalEquipments = new ArrayList<Equipment>();
	@Default
	private List<Equipment> importerDefinedDealerEquipments = new ArrayList<Equipment>();
	@Default
	private List<Equipment> dealerEquipments = new ArrayList<Equipment>();
	@Default
	private List<PrItem> vehiclePrItems = new ArrayList<PrItem>();
	@Default
	private Map<String, Set<String>> equipmentDependencyMap = new HashMap<>();
	private String deliveryDate;
	private String deliveryTime;
	private String deliveryWeek;
	private String deliveryYear;
	private String deliveryWeekDay;
	private boolean deliveryArranged;
	private String orderType;
	private Integer horsePower;
	@Default
	private VehicleCondition vehicleCondition = VehicleCondition.NOT_SPECIFIED;
	private String color;
	private String interior;
	private String kilometers;
	private String statusCode;
	private Integer type;
	private VehiclePurchasedByOwnerEconomy vehiclePurchasedByOwnerEconomy;
	private ServiceContract serviceContract;
	private ExtendedWarranty extendedWarranty;
	private FinanceOffer financeOffer;
	private InsuranceOffer insuranceOffer;
	private AllInclusive allInclusive;
	private LeaseOffer leaseOffer;
	@Default
	private List<ImporterEquipment> importerEquipments = new ArrayList<ImporterEquipment>();
	private Integer amount;
	private SalesChannel salesChannel;

}
