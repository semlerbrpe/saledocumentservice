package dk.semlerit.autocore.ws.saledocument.core.model;

import java.util.Optional;

import lombok.Getter;

/**
 * @author extbda
 * @author mi
 * @author lafl
 * @author extthtb
 * 
 * @version 3.0
 * @since 2.6.0
 */
@Getter
public enum VehicleCondition {
	NOT_SPECIFIED("-1"), NEW("0"), USED("1"), USED_DEMO("2"), USED_RENT("3");

	private String value;

	private VehicleCondition(final String value) {
		this.value = value;
	}

	public static VehicleCondition getByValue(String value) {

		for (final VehicleCondition vehicleConditionType : VehicleCondition.values()) {
			if (Optional.ofNullable(value)
					.orElse("-1")
					.equalsIgnoreCase(vehicleConditionType.value)) {
				return vehicleConditionType;
			}
		}
		throw new IllegalArgumentException("Value: '"
				+ value
				+ "' could not be translated into this enum type");
	}
}
